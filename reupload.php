<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
} 

?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pembayaran | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head><!--/head-->

<body>
	<?php include 'header.php' ?>

	<section id="pembayaran"><!--pembayaran-->
		<div class="container">
			<div class="step">
				<ul class="steps">
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pembayaran</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>2. Konfirmasi Pembayaran</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>3. Upload Bukti Pembayaran</li>
				  <li class="select">4. Selesai</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-sm-7 col-sm-offset-2">
					<div class="bayar">
						<p>Mohon maaf, Foto bukti transfer yang anda upload tidak valid. Pastikan hasil foto dapat terbaca dengan baik dan jelas, sehingga mempermudah kami dalam melakukan validasi. Silahkan melakukan proses upload foto bukti transfer yang anda miliki.</p>
						<a class="btn btn-default btn_bayar" href="checkout_done.php">Upload Ulang Bukti</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Sesuai Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>