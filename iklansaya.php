<?php 
include 'koneksi.php';
session_start(); 

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Product Details | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h1><font color="#9C9B9B">KURNIA </font><font color="#FE980F">KAMERA</font></h1></a>
						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
					            		<li><a href="checkout.php">
					            			<div class="mark">Pemesanan Kamera</div>
					            		</a></li>
					            		<li><a href="checkout_lelang.php">
					            			<div class="mark">Transaksi Lelang</div>
					            		</a></li> 
					            	</ul>
					          	</li>
								<li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
								<?php 
								/*if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';*
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}*/
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Beranda</a></li>
								<li class="dropdown"><a href="">Produk<i class="fa fa-angle-down"></i></a>
						            <ul role="menu" class="sub-menu">
						                <li><a href="index.php?kat=1">Biji kopi</a></li>
										<li><a href="index.php?kat=2">Mesin Kopi</a>
										</li> 
						            </ul>
						        </li> 
								<li><a href="lelang.php">Lelang Resep</a></li>
								<li><a href="layanan.php">Layanan Perbaikan</a></li>
								<li><a href="contact.php">Kontak</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Kategori</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="index.php?kat=1">DSLR</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="index.php?kat=2">MIRORLES</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					</div>
				</div>
				<?php
				include 'koneksi.php';

				$itemID = 0;

				if(isset($_GET['itemID']))
				{
					$item_id = $_GET['itemID'];
				}

				$sql = mysqli_query($mycon, "SELECT * FROM kamera WHERE id = '" .$item_id. "'");


				?>
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<?php
								while($row = mysqli_fetch_array($sql))
								{

									echo '<img src="../bismillah/images/' .$row['gambar']. '" alt="" /></a>';
								}
								?>
							</div>
						</div>
						
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<?php
								$sql = mysqli_query($mycon, "SELECT *, k.id as kamera_id FROM kamera k, kategori kat WHERE k.kategori_id = kat.id and k.id = '" .$item_id. "'");
								while($row = mysqli_fetch_array($sql))
								{
						  echo '<form method="POST" action="cart_proc1.php">

									<h2>' .$row['namakamera']. '</h2>';


							// echo 	'<label for="Durasi"> Durasi : </label>
							// 		<select id="cmbdurasi" name="durasi" >
							// 		   <option value="0">Pilih Durasi Sewa</option>
							// 		   <option value="'.$row['6jam'].'">6 jam </option>
							// 		   <option value="'.$row['12jam'].'">12 jam</option>
							// 		   <option value="'.$row['24jam'].'">24 jam</option>
							// 		</select>

							// 		<span>
							// 			<span id="hargasewa1"></span>';

							echo	'<span>
									<label>Quantity:</label>';										
       								if($row['stoktotal'] > 0)
									{
							  	  echo '<input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" min="1" max="' .$row['stoktotal']. '" name="jml_item" value="1"/>';
							      echo '<input type="hidden" name="item_ID" value="' .$row['kamera_id'].'"><br></br>';

							      echo '<input type="submit" class"btn btn-fefault" style="background: #FE980F; border: 0 none; border-radius: 20; color: #FFFFFF; font-size: 15px; margin-bottom: 10px; margin-left: 20px; width: 50%;"  name="jml" value="Add to Cart" />
							  		</span>
								</form>
								<p><b>Stok Barang : </b> Tersedia</p>';
									}
									else
									{
								  echo '<input type="number" onkeypress="return event.charCode >= 48" min="1" max="' .$row['stok']. '" disabled><br></br>
										<a href="index.php"><button type="button" style="width: 50%" class="btn btn-fefault cart" disabled>Add to cart</button></a>
									</span>
									<p><b>Stok Barang : </b>Habis</p>';
									}
									echo '</form>';
								}
								?>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li><a href="#details" data-toggle="tab">Details</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="details" >
								<?php
								$sql = mysqli_query($mycon, "SELECT * FROM kamera WHERE id = '" .$item_id. "'");
								while($row = mysqli_fetch_array($sql))
								{
									echo '<p>' .$row['deskripsi'] .'<p>';
								}
								?>
							</div>
						</div>
					</div><!--/category-tab-->
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript">
		var mycbo = document.getElementById('cmbdurasi');
		var hargasewa2 = document.getElementById('hargasewa1');
		var temp = '';

		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}

		mycbo.onchange = function(){
			// alert(this.value);
			// alert(formatRupiah(this.value, 'Rp. '));
			document.getElementById("hargasewa1").innerHTML = formatRupiah(this.value, 'Rp. ');
		}
	</script>
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretanny</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>