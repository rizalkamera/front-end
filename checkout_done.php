<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
	$qty = 0;
	
	//langsung tembak id pel 4
	$id_pel = 0;

	$id_nota = 0;
	 $jml = 0;
	 $bar = 0;
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];

	//cek apakah pelanggan punya transaksi yang statusnya menunggu bukti transfer
	$f = mysqli_query($mycon, "SELECT * FROM user p, notasewa n
	                           WHERE p.id = n.user_id AND p.username = '".$pengguna."' AND n.statusnota = 'menunggu data pembayaran' AND p.hapuskah = '0' AND  n.hapuskah = '0' AND n.statuskembali ='0'");
	
	$res = mysqli_num_rows($f);
	// if ($res < 1) {
	// 	echo '<script language="javascript">';
	//     echo 'document.location.href="index.php"';
	//     echo '</script>';
	// }

 if ($res < 1) {

	$f = mysqli_query($mycon, "SELECT * FROM user p, notasewa n
	                           WHERE p.id = n.user_id AND p.username = '".$pengguna."' AND n.statusnota = 'menunggu validasi pembayaran' AND p.hapuskah = '0' AND  n.hapuskah = '0' AND n.statuskembali ='0' AND n.statuspesanan = '0'");
	
	$row = mysqli_num_rows($f);
	//jika tidak ada, maka akan dialihkan ke beranda
	
	}
} ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pembayaran | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head><!--/head-->

<body>
	
<?php include 'header.php' ?>
	
	<section id="pembayaran"><!--pembayaran-->
		<div class="container">
			<div class="step">
				<ul class="steps">
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pimilihan Durasi Sewa</li>
				  <li class="select"><i class="fa fa-check-square-o"></i> 2.  Mengisi Form Data Sewa</li>
				  <li class="select" >3. Pilih Pembayaran</li>
				  <li >4. Upload Bukti Pembayaran</li>
				  <li >5. Selesai</li>
				</ul>
			</div>


							
			<form method="POST" action="upload_bukti.php">
				
				<?php
				$s2 = mysqli_query($mycon, "SELECT SUM(d.hargasewa*d.jmlsewa) as lunas, ROUND(SUM((d.hargasewa*d.jmlsewa)*0.5)) as dp FROM hub_notasewa_dan_kamera d, notasewa e, user p WHERE d.nota_id = e.id AND e.user_id = p.id AND p.username ='$pengguna' AND e.statuskembali = '0'");
				if(!$s2)
				{
				echo 's2 '. mysqli_error($mycon);
				}
				else
				{
					$row = mysqli_fetch_array($s2);
				}

				?>
				<div align="center"> <h2>Pilih Pembayaran</h2>
					<p>==>> pembayaran bisa Dp (pembayaran setengah harga) atau lunas <<== </p>
				<select style="width: 50%" id="cmbpembayaran" name="pemabayaran" required >
					 <option value="">Pilih Pembayaran</option>
						<option value="<?php echo $row['dp']?>">DP </option>
						<option value= "<?php echo $row['lunas']?>">Lunas </option>
				</select>
			</div>

			<div class="row">
				<div class="col-sm-10 col-sm-offset-2">
					<div class="bayar"><!--login form-->
						<div class="col-sm-7 col-sm-offset-1">
							<div class="detil_bayar">
								<p>Total transaksi yang harus anda bayar adalah</p>
								<strong><p style="margin-left: 30%">
									
								<td><span id='totalbayar'></span></td>

								</p></strong>
								<br>
								<!-- Silahkan Pilih Bank Tujuan Transfer yang Anda Inginkan<br></br> -->
								<div class="funkyradio" style="margin-left: 10%;">
									<form method="POST" action="upload_bukti.php">
									<input type="hidden" id="nominal_bayar" name="nominal_bayar" />
									<?php 
								
									?>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-9 col-sm-offset-2">
				<div class="bayar">
					<p>Jika anda telah memilih pembayaran , silahkan klik tombol Transfer  </p>
<!-- 					<a href="upload_bukti.php">SAYA SUDAH BAYAR</a> -->
					<button class="btn btn-default btn_bayar" type="submit" name="tuku">Lanjut proses transfer</a>
				</div>
					
			</div>
		</div>
			</form>
			
			
	</section><!--/form-->
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamere</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	<script type="text/javascript">
		var mycbo = document.getElementById('cmbpembayaran');
		var hargasewa2 = document.getElementById('hargasewa1');
		var temp = '';

		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}

		mycbo.onchange = function(){
			// alert(this.value);
			// alert(formatRupiah(this.value, 'Rp. '));
			document.getElementById("totalbayar").innerHTML = formatRupiah(this.value, 'Rp. ');
			document.getElementById("nominal_bayar").value = this.value;
		}
	</script>

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
<?php

// lalu ada link "lanjutkan transaksi" yang menuju ke checkout_done.php
