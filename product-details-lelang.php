<?php 
include 'koneksi.php';
session_start(); 

if(empty($_SESSION['aktif']))
{
	echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
}


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Product Details | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h1><font color="#9C9B9B">Kurnia </font><font color="#FE980F">Kamera</font></h1></a>
						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
					            		<li><a href="checkout.php">
					            			<div class="mark">Pembelian Barang</div>
					            		</a></li>
					            		<li><a href="checkout_lelang.php">
					            			<div class="mark">Transaksi Lelang</div>
					            		</a></li> 
					            	</ul>
					          	</li>
								<li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
								<?php 
								if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Beranda</a></li>
								<li class="dropdown"><a href="">Kamera<i class="fa fa-angle-down"></i></a>
						            <ul role="menu" class="sub-menu">
						                <li><a href="dslr.php">DSLR</a></li>
										<li><a href="mirorrles.php">MIRORRLES</a>
										<li><a href="index.php?kat=3">ACTION CAM</a>
										</li> 
						            </ul>
						        </li> 
						        <li><a href="lensa.php">Lensa</a></li>
						        <li><a href="asessoris.php">Asessoris</a></li>
								<li><a href="lelang.php">Lelang </a></li>
								<li><a href="jual.php">Jual</a></li>
								<li><a href="carasewa.php">Cara Sewa</a></li>
								<li><a href="contact.php">Kontak</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Kategori</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="dslr.php">Canon</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="index.php?kat=2">Nikon</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					</div>
				</div>
				<?php
				include 'koneksi.php';

				$lelang = 0;

				if(isset($_GET['id']))
				{
					$lelang = $_GET['id'];
				}


				$sql = mysqli_query($mycon, "SELECT * FROM lelang where hapuskah = '0' AND id = '" .$lelang. "'");
				?>
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product text-center">
								
								<?php
								while($row = mysqli_fetch_array($sql))
								{
									echo '<h2> '.$row['namabarang']. '</h2>';
									
									echo '<img src="../bismillah/images/' .$row['gambarbarang']. '" alt="" />';
								}
								?>
							</div>
						</div>
						
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<?php
								//SELECT l.id, l.pelanggan_id, p.nama FROM lelang l, user p WHERE l.pelanggan_id = p.id
								//$sql = mysqli_query($mycon, "SELECT l.id, l.namabarang, l.hargaawal, l.hargatertinggi, p.nama FROM lelang l, user p WHERE l.pelanggan_id = p.id ");
								$sql = mysqli_query($mycon, "SELECT * FROM lelang where hapuskah = '0' AND id = '" .$lelang. "'");
								while($row = mysqli_fetch_array($sql))
								{
									echo '<form method="POST" action="product-details-lelang.php">';
									echo '<input type="hidden" id="idlelang" name="idlelang" value="' .$row['id'].'"/>';

									echo '<span>Harga Awal : Rp. ' .number_format($row['hargaawal'], 0, ',', '.').',- <br></br>
									<input type="hidden" name="awal" value="' .$row['hargaawal'].'">


									Penawaran Tertinggi Saat Ini <h5><font color="#DC143C"> ' .$row['namapenawar']. '</font><h5>
									<h3>Rp. ' .number_format($row['hargatertinggi'], 0, ',', '.').',-'  . '<h3>
									<input type="hidden" name="tertinggi" value="' .$row['hargatertinggi'].'">

									';
									
									echo '</span>
									<div style="color: black; font-size: 16px;">Masukkan penawaran anda </div>
									<input type="number"  name="hrga_tawar" style="width: 65%;"/>';
										
										// echo '<a href="cart_proc.php?itemID=' .$row['id_barang'].'"><input type="submit" class"btn btn-fefault" style="background: #FE980F; border: 0 none; border-radius: 20; color: #FFFFFF; font-size: 15px; margin-bottom: 10px; margin-left: 20px; width: 23%;"  name="jml" value="Add to Cart" /></a>';

									echo '<input type="submit" class"btn btn-fefault" style="background: #FE980F; border: 0 none; border-radius: 20; color: #FFFFFF; font-size: 15px; margin-bottom: 10px; margin-left: 20px; width: 23%;"  name="sub" value="Tawar" />';

									echo '</form>';
								}


								?>


							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#details" data-toggle="tab">Details</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="details" >
								<?php
								$sql = mysqli_query($mycon, "SELECT * FROM lelang WHERE id = '" .$lelang. "'");
								while($row = mysqli_fetch_array($sql))
								{
									echo '<p>' .$row['deskripsi'] .'<p>';
								}

								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>


<?php
if(isset($_POST['sub']))
{
	$id = $_POST['idlelang'];
	$h_awal = $_POST['awal'];
	$h_tinggi= $_POST['tertinggi'];
	$h_tawar = $_POST['hrga_tawar'];

	//cek apakah status pelanggan blacklist atau tidak
	$b = mysqli_query($mycon, "SELECT * FROM user WHERE username = '" .$pengguna. "'");
	$res_b = mysqli_fetch_array($b);
	$black = $res_b['blacklist'];
	if($black == 1)
	{
		echo '<script language="javascript"> 
          alert("Maaf, Anda masuk dalam daftar Blacklist kami. Anda baru dapat mengikuti event lelang ketika status anda telah kami hapus dari daftar blacklist. ")
          document.location.href="lelang.php"
          </script>';
	}
	else
	{
		//cek apakah lelang masih berjalan atau tidak
		//1. ambil data selesai lelang
		$g = mysqli_query($mycon, "SELECT * FROM lelang WHERE id = '" .$id. "'");
		$res_g = mysqli_fetch_array($g);
		$waktu = $res_g['tanggalakhir'];

		//2. ambil waktu saat ini
		date_default_timezone_set('Asia/Bangkok');
	  	$tgl = date('Y-m-d H:i:s');

	  	//3. bandingkan
		if($tgl > $waktu)
		{
			echo '<script language="javascript"> 
	          alert("Maaf, lelang telah berakhir. Silahkan berpartisipasi di event lelang yang aka datang. ")
	          document.location.href="lelang.php"
	          </script>';
		}

		else
		{
			//sebelum cek inputan, cek apakah pelanggan ini ada transaksi lelang yang belum diselesaikan
			// $m = mysqli_query($mycon, "SELECT * FROM lelang l, user p WHERE l.pelanggan_id = p.id AND l.statuspembayaran != 'selesai' AND p.username = '" .$pengguna. "' AND l.hapuskah = '0' AND p.hapuskah = '0'");
			$m = mysqli_query($mycon, "SELECT * FROM lelang l, user p WHERE l.pelanggan_id = p.id AND  l.statuspembayaran != 'selesai' AND p.username = '".$pengguna."' AND  l.hapuskah = '0' AND p.hapuskah ='0'	");
			$q = mysqli_num_rows($m);
			if($q > 0)	
			{
				echo '<script language="javascript">
					var id = "' .$id. '";	 
		          alert("Maaf, anda masih mempunyai transaksi lelang yang belum selesai. Anda dapat mengikuti lelang setelah semua transaksi telah selesai.")
		          document.location.href="product-details-lelang.php?id="+id
		          </script>';
			}
			else
			{
				if($h_tinggi == 0)
				{
					if($h_tawar <= $h_awal)
					{
						echo '<script language="javascript"> 
						var id = "' .$id. '";	
			          alert("Harga yang anda masukkan harus lebih besar dari harga awal.")
			          document.location.href="product-details-lelang.php?id="+id
			          </script>';
					}
					else
					{
						//set tanggal penawaran
						date_default_timezone_set('Asia/Bangkok');
		  				$tgl = date('Y-m-d H:i:s');

						//update harga_tertinggi
						$r = mysqli_query($mycon, "UPDATE lelang SET hargatertinggi = '" .$h_tawar. "' WHERE id = '" .$id. "'");

						//update tgl penawaran dgn datetiime saat ini
						$w = mysqli_query($mycon, "UPDATE lelang SET tanggalpenawaran = '" .$tgl. "' WHERE id = '" .$id. "'");

						//update pelanggan id di lelang_resep
						$u = mysqli_query($mycon, "SELECT * FROM user  WHERE  username = '" .$pengguna. "'");
						while($e = mysqli_fetch_array($u))
						{
							$idpel = $e['id'];
						}
						$w = mysqli_query($mycon, "UPDATE lelang SET pelanggan_id = '" .$idpel. "' WHERE id = '" .$id. "'");

						$a = mysqli_query($mycon, "UPDATE lelang SET namapenawar = '" .$pengguna. "' WHERE id = '" .$id. "'");
						
						$b = mysqli_query($mycon, "UPDATE lelang SET statuspembayaran = 'menunggu pembayaran' WHERE id = '" .$id. "'");
}



						echo '<script language="javascript"> 
			          var id = "' .$id. '";	
			          alert("Penawaran berhasil dilakukan.")
			          document.location.href="product-details-lelang.php?id="+id
			          </script>';
					}
				
				else
				{
					if($h_tawar <= $h_tinggi)
					{
						echo '<script language="javascript"> 
			          var id = "' .$id. '";	
			          alert("Harga yang anda masukkan harus lebih besar dari harga tertinggi.")
			          document.location.href="product-details-lelang.php?id="+id
			          </script>';
					}
					else
					{
						//update harga tertinggi lelang tsb

						//set tanggal penawaran
						date_default_timezone_set('Asia/Bangkok');
		  				$tgl = date('Y-m-d H:i:s');

						//update harga_tertinggi
						$r = mysqli_query($mycon, "UPDATE lelang SET hargatertinggi = '" .$h_tawar. "' WHERE id = '" .$id. "'");

						//update tgl penawaran dgn datetiime saat ini
						$w = mysqli_query($mycon, "UPDATE lelang SET tanggalpenawaran = '" .$tgl. "' WHERE id = '" .$id. "'");

						//update pelanggan id di lelang_resep
						$u = mysqli_query($mycon, "SELECT id FROM user WHERE username = '" .$pengguna. "'");
						while($e = mysqli_fetch_array($u))
						{
							$idpel = $e['id'];
						}
						$w = mysqli_query($mycon, "UPDATE lelang SET pelanggan_id = '" .$idpel. "' WHERE id = '" .$id. "'");

						$a = mysqli_query($mycon, "UPDATE lelang SET namapenawar = '" .$pengguna. "' WHERE id = '" .$id. "'");

						$b = mysqli_query($mycon, "UPDATE lelang SET statuspembayaran = 'menunggu pembayaran' WHERE id = '" .$id. "'");


						echo '<script language="javascript"> 
			          var id = "' .$id. '";	
			          alert("Penawaran berhasil dilakukan.")
			          document.location.href="product-details-lelang.php?id="+id
			          </script>';
					}
				}	
			}
		}
	}	
}


?>