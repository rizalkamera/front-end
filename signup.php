<!DOCTYPE html>
<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Signup | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h1><font color="#000000">Kurnia </font><font color="#FE980F">Kamera</font></h1></a>
						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
					            		<li><a href="checkout.php">
					            			<div class="mark">Pemesanan Barang</div>
					            		</a></li>
					            		<li><a href="checkout_lelang.php">
					            			<div class="mark">Transaksi Lelang</div>
					            		</a></li> 
					            	</ul>
					          	</li>
								<li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
								<?php 
								if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Beranda</a></li>
								<li class="dropdown"><a href="">Kamera<i class="fa fa-angle-down"></i></a>
						            <ul role="menu" class="sub-menu">
						                <li><a href="dslr.php">DSLR</a></li>
										<li><a href="mirorrles.php">MIRORRLES</a>
										<li><a href="index.php?kat=3">ACTION CAM</a>
										</li> 
						            </ul>
						        </li> 
						        <li><a href="lensa.php">Lensa</a></li>
						        <li><a href="asessoris.php">Asessoris</a></li>
								<li><a href="lelang.php">Lelang </a></li>
								<li><a href="jual.php">Jual</a></li>
								<li><a href="carasewa.php">Cara Sewa</a></li>
								<li><a href="contact.php">Kontak</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div style="align: center;" class="col-sm-6 col-sm-offset-4">
					<div class="signup-form"><!--sign up form-->
						
						<a href="index.php"><h2><font style="margin-left: 15%;" color="#000000">Kurnia </font><font color="#FE980F">Kamera</font> <font color="#9932CC">Daftar</font></h2></a>
						<h4 style="margin-left: 20%;">Daftar Sebagai Pelanggan Baru</h4>
						<form method="POST" action="signup.php">
								
							<input style="text-align: center;" type="text" name="fname" style="width: 50%;" placeholder="Nama Depan" required  />
							<input type="text" name="lname" style="width: 50%;" placeholder="Nama Belakang" required />
							
							<textarea rows="3" cols="50" name="address" placeholder=" Alamat" required></textarea>
							<br></br>
							<input type="text" name="phone" style="width: 70%;" placeholder="No HP" required/>
							<input type="email" name="email" style="width: 70%;" placeholder="Email" required/>
							<input type="text" name="user" style="width: 70%;" placeholder="Nama Pengguna" required/>
							<input type="password" name="pw" style="width: 70%;" placeholder="Password" required/>
							<input type="password" name="repass" style="width: 70%;" placeholder="Ulangi Password" required/>

							<br></br>
							
							<input type="submit" name="signup" class="btn btn-default" style="background:#FE980F; color:#FFF; display: inline; margin-left: 0%; font-size: 16px; font-weight: bold; width:70%;"  value="Daftar"/>

						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<?php

include 'koneksi.php';

if(isset($_POST['signup']))
{
	if($_POST["pw"] != $_POST["repass"])
	{
		echo '<script language="javascript">'. 
			'window.alert("Maaf, Password yang anda masukan tidak cocok. Silahkan ulangi kembali")'.
			'</script>';
	}
	else if($_POST["pw"] == $_POST["repass"]) {
	    $password = $_POST["pw"];
	    $cpassword = $_POST["repass"];
	    if (strlen($_POST["pw"]) < '8') {
	        // $passwordErr = "Your Password Must Contain At Least 8 Characters!";
	        echo '<script language="javascript">'. 
			'window.alert("Maaf, Password setidaknya memiliki panjang 8 karakter.")'.
			'</script>';
	    }
	    elseif(!preg_match("#[0-9]+#",$password)) {
	        // $passwordErr = "Your Password Must Contain At Least 1 Number!";
	        echo '<script language="javascript">'. 
			'window.alert("Maaf, Password setidaknya mengandung 1 digit angka.")'.
			'</script>';
	    }
	    elseif(!preg_match("#[A-Z]+#",$password)) {
	        // $passwordErr = "Your Password Must Contain At Least 1 Capital Letter!";
	        echo '<script language="javascript">'. 
			'window.alert("Maaf, Password setidaknya mengandung 1 huruf besar.")'.
			'</script>';
	    }
	    elseif(!preg_match("#[a-z]+#",$password)) {
	        // $passwordErr = "Your Password Must Contain At Least 1 Lowercase Letter!";
	        echo '<script language="javascript">'. 
			'window.alert("Maaf, Password setidaknya mengandung 1 huruf kecil.")'.
			'</script>';
	    }
	    else
		{
			$fname = $_POST['fname'];
			$lname = $_POST['lname'];
			$uname = $_POST['user'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$pass = md5($_POST['pw']);

			//pengecekan username sudah terdaftar atau blm
			$sql = mysqli_query($mycon, "SELECT * from user where username = '" .$uname. "' AND hapuskah = '0'");
			$cek_data = mysqli_num_rows($sql);
			if($cek_data > 0)
			{
				echo '<script language="javascript">'. 
				'window.alert("Maaf, username sudah digunakan. Coba dengan username yang lain.")'.
				'</script>';
			}

			
			else if($cek_data < 1)
			{
				echo '<script language="javascript">'. 
				'window.alert("Username avalable")'.
				'</script>';

				//pengecekan email sudah terdaftar atau belum
				$sqla = mysqli_query($mycon, "SELECT * from user where email = '$email' AND hapuskah = '0'");
				$cek_dataq = mysqli_num_rows($sqla);
				if($cek_dataq == 1)
				{
					echo '<script language="javascript">'. 
					'window.alert("Maaf, email sudah digunakan. Coba dengan email yang lain.")'.
					'</script>';
				}
				else
				{
					echo '<script language="javascript">'. 
					'window.alert("email avalable")'.
					'</script>';

					$name = $fname . ' ' . $lname;

					$sql = mysqli_query($mycon, "INSERT INTO user(nama, email, notlp, username, password, hapuskah) VALUES('$name', '$email', '$phone','$uname','$pass','0')");
					if(!$sql) {
						echo '1 '.mysqli_error($mycon);
					}

					//'document.location.href="login.php"'.
					
					echo '<script language="javascript">'. 
					'window.alert("Data berhasil terinput. Klik OK untuk Masuk.");'.
				    
				    '</script>';
				}
			}
		}
	}
}

?>