-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2019 at 04:28 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta_kameras`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `idbank` int(11) NOT NULL,
  `namabank` varchar(20) NOT NULL,
  `norekening` varchar(20) NOT NULL,
  `atasnama` varchar(11) NOT NULL,
  `hapuskah` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`idbank`, `namabank`, `norekening`, `atasnama`, `hapuskah`) VALUES
(1, 'BCA', '0889091789', 'kurnia', 0),
(2, 'MANDIRI', '1234455009988', 'kurniakmera', 0);

-- --------------------------------------------------------

--
-- Table structure for table `datapesanan`
--

CREATE TABLE `datapesanan` (
  `id` int(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamatlengkap` varchar(50) NOT NULL,
  `notlpn` int(13) NOT NULL,
  `email` varchar(20) NOT NULL,
  `tanggalpakai` datetime DEFAULT NULL,
  `tanggalkembali` datetime NOT NULL,
  `hapuskah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datapesanan`
--

INSERT INTO `datapesanan` (`id`, `nama`, `alamatlengkap`, `notlpn`, `email`, `tanggalpakai`, `tanggalkembali`, `hapuskah`) VALUES
(1, 'rizal kurniawan', 'jl.tenggilis mejoyo surabaya', 12345, ' sad', '2019-04-19 01:00:00', '2019-04-22 01:00:00', 0),
(2, 'rizal kurniawan', 'jl.tenggilis mejoyo surabaya', 12345, ' sad', '2019-04-19 01:00:00', '2019-04-22 01:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hub_notasewa_dan_kamera`
--

CREATE TABLE `hub_notasewa_dan_kamera` (
  `nota_id` int(11) NOT NULL,
  `kamera_id` int(11) NOT NULL,
  `rusak` int(11) NOT NULL,
  `hilang` int(11) NOT NULL,
  `denda` int(11) NOT NULL,
  `hargasewa` int(11) DEFAULT NULL,
  `tgl_ambil` datetime NOT NULL,
  `tgl_kembali` datetime NOT NULL,
  `durasi` int(2) NOT NULL,
  `kondisikamera` varchar(20) NOT NULL,
  `jmlsewa` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hub_notasewa_dan_kamera`
--

INSERT INTO `hub_notasewa_dan_kamera` (`nota_id`, `kamera_id`, `rusak`, `hilang`, `denda`, `hargasewa`, `tgl_ambil`, `tgl_kembali`, `durasi`, `kondisikamera`, `jmlsewa`) VALUES
(41, 6, 0, 0, 0, 170000, '2019-10-02 11:00:42', '2019-10-09 10:00:40', 24, '', 1),
(42, 60, 0, 0, 0, 20000, '2019-10-08 10:00:40', '2019-10-09 10:00:40', 24, '', 1);

--
-- Triggers `hub_notasewa_dan_kamera`
--
DELIMITER $$
CREATE TRIGGER `kurangi_stok` AFTER INSERT ON `hub_notasewa_dan_kamera` FOR EACH ROW update kamera set stoktotal = stoktotal-NEW.jmlsewa
WHERE id = NEW.kamera_id
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tambah_stok` AFTER DELETE ON `hub_notasewa_dan_kamera` FOR EACH ROW UPDATE kamera set stoktotal = stoktotal + OLD.jmlsewa
where id = OLD.kamera_id
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_stok` BEFORE UPDATE ON `hub_notasewa_dan_kamera` FOR EACH ROW BEGIN
IF(NEW.jmlsewa < OLD.jmlsewa) THEN
UPDATE kamera set stoktotal = stoktotal + (OLD.jmlsewa - NEW.jmlsewa)
where id = OLD.kamera_id;
ELSEIF(NEW.jmlsewa > OLD.jmlsewa) THEN
UPDATE kamera set stoktotal = stoktotal - (NEW.jmlsewa - OLD.jmlsewa)
WHERE id = OLD.kamera_id;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `hub_user_dan_lelang`
--

CREATE TABLE `hub_user_dan_lelang` (
  `id user` int(11) NOT NULL,
  `id lelang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kamera`
--

CREATE TABLE `kamera` (
  `id` int(11) NOT NULL,
  `namakamera` varchar(45) NOT NULL,
  `merekkamera` varchar(50) NOT NULL,
  `namatipe` varchar(45) NOT NULL,
  `harga_6jam` int(11) NOT NULL,
  `harga_12jam` int(11) NOT NULL,
  `harga_24jam` int(11) NOT NULL,
  `stoktotal` int(11) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `gambar` varchar(95) NOT NULL,
  `hargabeli` int(11) NOT NULL,
  `hapuskah` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamera`
--

INSERT INTO `kamera` (`id`, `namakamera`, `merekkamera`, `namatipe`, `harga_6jam`, `harga_12jam`, `harga_24jam`, `stoktotal`, `deskripsi`, `gambar`, `hargabeli`, `hapuskah`, `kategori_id`) VALUES
(6, 'CANON 3000 D', '', 'DSLR', 80000, 120000, 170000, 4, 'kamera favorit', '23f9854322.jpg', 8000000, 0, 1),
(59, 'CANON EOS 200D ', '', 'DSLR', 200000, 250000, 300000, 3, 'Resolusi 24.2 ', '61f4ff0406.jpg', 15000000, 0, 1),
(60, 'CANON 50 D', '', 'DSLR', 100000, 150000, 20000, 1, 'kamera sama lensa kebalik', '1c8e2d15e1.jpg', 15000000, 0, 1),
(64, 'LENSA FIX', '', 'canon', 15000, 20000, 30000, 2, 'lensa ok boss', 'a777c45a7e.jpg', 2000000, 0, 2),
(65, 'LENSA TELE', '', 'canon', 40000, 60000, 70000, 3, 'Lensa Multifungsi Jarak Jauh Dekat Ok', '41b9f62e97.jpg', 5000000, 0, 2),
(66, 'LENSA FISHEYE', '', 'LENSA', 70000, 85000, 100000, 4, '- Aperture Range: \r\n', 'ed15ec1a13.jpg', 4000000, 0, 2),
(67, 'CANON EF 50mm F/1.8 STM', '', 'LENSA', 20000, 35000, 50000, 5, 'Canon EF 50mm f/1.8 STM adalah salah satu focal length yang paling serbaguna dan banyak tersedi', 'c8f41dd5e2.jpg', 1500000, 0, 2),
(69, 'Canon EOS 1500D', '', 'KAMERA', 70000, 10000, 13000, 2, 'Canon EOS 1500D Kit EF-S 18-55mm f/3.5-5.6 IS II', '8c87af7d16.jpg', 5000000, 0, 1),
(70, 'Attanta TVT-224MT Tripod', '', 'TRIPOD', 15000, 30000, 40000, 2, 'Tripod & Monopod Conversion\r\nUnlock & Pull, 4 Leg Extension\r\nBall Head BH-30B\r\nPanjang terlipat', '3cdcf0f38f.jpg', 650000, 0, 3),
(72, 'Sandisk 90Mb/S â€“ 128GB', '', 'Asessoris', 15000, 20000, 30000, 1, 'Memiliki kapasitas 128GB\r\nUHS-I / V30 / U3 /Class 10\r\nMaksimal Read Speed: 90 MB/s\r\nMaksimal Wr', 'c062ffb363.jpg', 100000, 0, 3),
(73, 'CANON 4000 D', '', 'DSLR', 80000, 120000, 160000, 2, '18MP APS-C Sensor\r\nScene Intelligent Auto\r\nSupport Wi-Fi\r\nOptical Viewfinder\r\nFullHD EOS Movies', 'd13206bc8c.jpg', 4000000, 0, 1),
(77, 'Lensa YongNuo Canon 50mm F1.8 Mark II', '', 'LENSA', 25000, 35000, 50000, 3, 'Canon EF-Mount/Full-', '12ea6bb836.jpg', 1150000, 0, 2),
(79, 'FLASH YongNuo 565EX III', '', 'Asessoris ', 20000, 30000, 40000, 5, 'Kompatibel dengan C', '5380bfd06c.jpg', 1165000, 0, 3),
(84, 'kanon 560 D', '', 'kamera', 0, 0, 0, 0, 'kamera ok', 'ae6d86400f.jpg', 0, 0, 6),
(98, 'm10', 'Canon', 'Mirorrles', 50000, 80000, 10000, 2, 'ok coba', 'e07995ce1a.jpg', 5000000, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `hapuskah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `hapuskah`) VALUES
(1, 'kamera', 0),
(2, 'lensa', 0),
(3, 'aksesoris', 0),
(6, 'lelang', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lelang`
--

CREATE TABLE `lelang` (
  `id` int(11) NOT NULL,
  `namabarang` varchar(50) NOT NULL,
  `namapenawar` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `hargaawal` int(11) NOT NULL,
  `hargatertinggi` int(20) NOT NULL,
  `tanggalawal` datetime NOT NULL,
  `tanggalakhir` datetime NOT NULL,
  `tanggalpenawaran` datetime NOT NULL,
  `gambarbarang` varchar(100) NOT NULL,
  `statuspembayaran` varchar(50) NOT NULL,
  `namapemilik` varchar(20) NOT NULL,
  `nominaltransfer` int(11) NOT NULL,
  `buktivalid` varchar(11) NOT NULL,
  `buktitransfer` varchar(60) NOT NULL,
  `norek` varchar(20) NOT NULL,
  `emailtujuan` varchar(50) NOT NULL,
  `id_kamera` int(11) DEFAULT NULL,
  `pelanggan_id` int(10) NOT NULL,
  `bankid` int(10) NOT NULL,
  `pembayaran_id` int(20) NOT NULL,
  `hapuskah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lelang`
--

INSERT INTO `lelang` (`id`, `namabarang`, `namapenawar`, `jenis`, `deskripsi`, `hargaawal`, `hargatertinggi`, `tanggalawal`, `tanggalakhir`, `tanggalpenawaran`, `gambarbarang`, `statuspembayaran`, `namapemilik`, `nominaltransfer`, `buktivalid`, `buktitransfer`, `norek`, `emailtujuan`, `id_kamera`, `pelanggan_id`, `bankid`, `pembayaran_id`, `hapuskah`) VALUES
(99, 'canon 600 D', 'moza sakira', '', 'ok                                             ', 300000, 400000, '2019-09-30 11:15:00', '2019-09-30 11:17:00', '2019-09-30 11:15:13', 'f6d89f8284.jpg', 'menunggu bukti transfer', 'moza', 400000, '', 'c3798b21e7.jpg', '1235432', '', 1, 55, 1, 0, 0),
(100, 'Lensa FISHEYE', 'sonya arista', '', 'ok                                             ', 300000, 400000, '2019-09-30 11:25:00', '2019-09-30 11:27:00', '2019-09-30 11:25:15', 'f52528c461.jpg', 'menunggu bukti transfer', '', 0, '', '', '', '', 2, 54, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notasewa`
--

CREATE TABLE `notasewa` (
  `id` int(11) NOT NULL,
  `namapenyewa` varchar(20) DEFAULT NULL,
  `tanggalpesan` datetime NOT NULL,
  `pembayaran` varchar(32) DEFAULT NULL,
  `notlpn` int(13) DEFAULT NULL,
  `persyaratan` varchar(100) DEFAULT NULL,
  `statusnota` varchar(50) NOT NULL,
  `dendaterlambat` int(11) NOT NULL,
  `dendakerusakan` int(11) NOT NULL,
  `kondisikamera` varchar(20) NOT NULL,
  `dp` int(50) NOT NULL,
  `pelunasan` varchar(45) NOT NULL,
  `statuspesanan` varchar(20) NOT NULL,
  `grandtotal` int(50) NOT NULL,
  `buktitransfer` varchar(100) NOT NULL,
  `bankid` int(20) DEFAULT NULL,
  `norekening` int(20) DEFAULT NULL,
  `namapemilik` varchar(20) DEFAULT NULL,
  `nominaltransfer` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `hapuskah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notasewa`
--

INSERT INTO `notasewa` (`id`, `namapenyewa`, `tanggalpesan`, `pembayaran`, `notlpn`, `persyaratan`, `statusnota`, `dendaterlambat`, `dendakerusakan`, `kondisikamera`, `dp`, `pelunasan`, `statuspesanan`, `grandtotal`, `buktitransfer`, `bankid`, `norekening`, `namapemilik`, `nominaltransfer`, `user_id`, `hapuskah`) VALUES
(41, NULL, '2019-10-02 10:28:11', NULL, NULL, 'SIM dan STNK', 'menunggu validasi pembayaran', 0, 0, '', 0, 'belum lunas', '', 170000, 'a6fafbc5e9.jpg', 1, 2311111, 'moza', 85000, 55, 0),
(42, NULL, '2019-10-02 20:52:06', NULL, NULL, 'SIM dan STNK', 'menunggu validasi pembayaran', 0, 0, '', 0, 'belum lunas', '', 20000, 'cf92c876ea.jpg', 1, 12344, 'rizal kurniawan', 20000, 53, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaranlelang`
--

CREATE TABLE `pembayaranlelang` (
  `id` int(20) NOT NULL,
  `atasnama` varchar(45) DEFAULT NULL,
  `norekening` int(20) DEFAULT NULL,
  `buktitransfer` varchar(100) DEFAULT NULL,
  `bankasal` varchar(11) DEFAULT NULL,
  `nominaltransfer` int(30) DEFAULT NULL,
  `bankid` varchar(20) DEFAULT NULL,
  `lelang_id` int(10) DEFAULT NULL,
  `hapuskah` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `postinganjualkamera`
--

CREATE TABLE `postinganjualkamera` (
  `id` int(11) NOT NULL,
  `namaproduk` varchar(100) NOT NULL,
  `kondisikamera` varchar(15) NOT NULL,
  `notlppenjual` varchar(15) DEFAULT NULL,
  `kategori` varchar(45) NOT NULL,
  `deskripsi` varchar(600) NOT NULL,
  `spesifikasi` varchar(30) NOT NULL,
  `hargajual` int(20) DEFAULT NULL,
  `foto` varchar(100) NOT NULL,
  `foto2` varchar(50) NOT NULL,
  `foto3` varchar(50) NOT NULL,
  `foto4` varchar(100) NOT NULL,
  `stok kamera` int(11) NOT NULL,
  `status` varchar(2) DEFAULT NULL,
  `iduser` int(10) NOT NULL,
  `hapuskah` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `postinganjualkamera`
--

INSERT INTO `postinganjualkamera` (`id`, `namaproduk`, `kondisikamera`, `notlppenjual`, `kategori`, `deskripsi`, `spesifikasi`, `hargajual`, `foto`, `foto2`, `foto3`, `foto4`, `stok kamera`, `status`, `iduser`, `hapuskah`) VALUES
(82, 'Canon 700 D Muranh Pakek Banget !!', 'Baru', '085259380479', 'Kamera', 'Bismillah Di Jual Canon 700 D \r\nMinus Pemakaian, Kondisi 97 %\r\nKelengkapan Seperti di Foto\r\n.\r\n.\r\nHub : 089611844167                                                                                                                                                                                                                                                                                                ', '', 5000000, 'fa96f412e7.jpg', '5c7e758000.jpg', '', '', 0, '1', 55, 0),
(84, 'Sony A-6000 Murah Banget', 'Baru', '085123456900', 'Kamera', ' iya                                                                                                ', '', 500000, '837030d469.jpg', 'd695aaf73e.jpg', '', '', 0, '1', 53, 0),
(85, 'Canon 60 D Kit + 18 - 55 mm', 'Bekas', '085123456900', 'Kamera', ' kamera 60D                                                                                                ', '', 50000, '1b8e824f97.jpg', '1b8e824f97.jpg', '', '', 0, NULL, 53, 0),
(86, 'canon 1300 D', 'Bekas', '085259380479', 'Kamera', ' ok                                                                                                ', '', 300000, '6c75694c99.jpg', 'fc0a675a35.jpg', '', '', 0, NULL, 55, 0),
(87, 'Canon 600 D Murah Pakek Banget !!', 'Bekas', '085123456900', 'Kamera', 'Di Jual Canon 600 D Masih Bagus\r\nKondisinya Normal 100 %\r\nSiapa Cepat Dia Dapat\r\n\r\nWa Fast Responds !!                                                                                                                                                                ', '', 3600000, '2b5451337f.jpg', 'cffdf4ff43.jpg', 'cc7d46821b.jpg', 'a6501180b4.jpg', 0, '1', 53, 0);

-- --------------------------------------------------------

--
-- Table structure for table `testimoni`
--

CREATE TABLE `testimoni` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` longtext NOT NULL,
  `gambar` varchar(45) NOT NULL,
  `id user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `notlp` varchar(15) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `hapuskah` int(11) NOT NULL,
  `idlelang` int(11) NOT NULL,
  `idnota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `alamat`, `email`, `jabatan`, `notlp`, `username`, `password`, `hapuskah`, `idlelang`, `idnota`) VALUES
(1, 'Sonya Arista', 'Jl. Slamet Riyadi no. 13', 'sonya@gmail.com', 'Kepala Kamera', '989777772', '0', '0', 0, 0, NULL),
(4, 'Rizal Kurniawan', 'Jl.Suekarno Hatta', 'rizalk771@gmail.com', '', '896118442', '0', '0', 0, 0, NULL),
(39, 'Rizal Kurniawan', 'jl.sukarno hatta no 46', 'rizalk771@gmail.com', 'Karyawan', '896118441', '0', '0', 0, 0, NULL),
(49, 'rizal kurniawan', 'Jl.kurnia kamera no 13', '', 'Kepala Kamera', '089611844167', '', '0', 0, 0, NULL),
(50, 'Tole', 'jl.sukarno', '', 'Kepala Teknisi', '085259380479', '', '0', 0, 0, NULL),
(51, 'rizal', 'jl.merbabu', 'ristarizal@gmail.com', '', '089511844167', 'rizal arista', '949', 0, 0, NULL),
(52, 'rizalkurniawan', 'jl.merbabu no 2', 'ristarizall@gmail.com', '', '089511844168', 'rizalkurniawan arista', '949f8ece3b56cbfdb740885578118421', 0, 0, NULL),
(53, 'rizal kurniawan', '', 'rizalk463@gmail.com', '', '085123456900', 'rizal', '949f8ece3b56cbfdb740885578118421', 0, 0, NULL),
(54, 'sonya', 'Jl.selamet riyadi no 17 probolinggo', 'sonyaarista@gmail.com', '', '089511844167', 'sonya arista', '48b36aebbb48af014513d282fcf634f9', 0, 0, NULL),
(55, 'moza', 'Jl, menuju rumah no 12 kota probolinggo', 'mozasyakira22@gmail.com', '', '085259380479', 'moza sakira', '67323faadd172a49b5d7873808167935', 0, 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`idbank`);

--
-- Indexes for table `datapesanan`
--
ALTER TABLE `datapesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hub_notasewa_dan_kamera`
--
ALTER TABLE `hub_notasewa_dan_kamera`
  ADD PRIMARY KEY (`nota_id`,`kamera_id`);

--
-- Indexes for table `hub_user_dan_lelang`
--
ALTER TABLE `hub_user_dan_lelang`
  ADD PRIMARY KEY (`id user`,`id lelang`),
  ADD UNIQUE KEY `id user` (`id user`),
  ADD UNIQUE KEY `id lelang` (`id lelang`);

--
-- Indexes for table `kamera`
--
ALTER TABLE `kamera`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lelang`
--
ALTER TABLE `lelang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notasewa`
--
ALTER TABLE `notasewa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `pembayaranlelang`
--
ALTER TABLE `pembayaranlelang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postinganjualkamera`
--
ALTER TABLE `postinganjualkamera`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id user` (`id user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `idbank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `datapesanan`
--
ALTER TABLE `datapesanan`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kamera`
--
ALTER TABLE `kamera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lelang`
--
ALTER TABLE `lelang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `notasewa`
--
ALTER TABLE `notasewa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `pembayaranlelang`
--
ALTER TABLE `pembayaranlelang`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `postinganjualkamera`
--
ALTER TABLE `postinganjualkamera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
