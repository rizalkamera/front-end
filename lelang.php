	<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
} 

$s = mysqli_query($mycon, "SELECT * FROM lelang where  hapuskah = '0'");

$cek_data = mysqli_num_rows($s);
//jika tidak ada 
if($cek_data < 1)
{
	// echo '<script language="javascript">';
 //    echo 'document.location.href="lelang_empty.php"';
 //    echo '</script>';
}
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Lelang Barang | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<script src="js/jquery.js"></script>
        
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head><!--/head-->

<body>
	<header id="header"><!--header-->		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h2><font color="#0000">KURNIA </font><font color="#FE980F">KAMERA </font><font color="#0000">LELANG</font></h1></a>
						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
					            		<li><a href="checkout.php">
					            			<div class="mark">Pemesana Barang</div>
					            		</a></li>
					            		<li><a href="checkout_lelang.php">
					            			<div class="mark">Transaksi Lelang</div>
					            		</a></li> 
					            	</ul>
					          	</li>
								<li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
								<?php 
								if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Beranda</a></li>
								<li class="dropdown"><a href="">Kamera<i class="fa fa-angle-down"></i></a>
						            <ul role="menu" class="sub-menu">
						                <li><a href="dslr.php">DSLR</a></li>
										<li><a href="mirorrles.php">MIRORRLES</a>
										<li><a href="index.php?kat=3">ACTION CAM</a>
										</li> 
						            </ul>
						        </li> 
						        <li><a href="lensa.php">Lensa</a></li>
						        <li><a href="asessoris.php">Asessoris</a></li>
								<li><a href="lelang.php">Lelang </a></li>
								<li><a href="jual.php">Jual</a></li>
								<li><a href="carasewa.php">Cara Sewa</a></li>
								<li><a href="contact.php">Kontak</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Beranda</a></li>
				  <li class="active">Lelang Barang</li>
				</ol>
			</div><!--/breadcrums-->
			<div class="row">
				
				<div class="col-sm-10 padding-right col-sm-offset-1">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Barang yang Dilelang</h2>
						<?php
							include 'koneksi.php';


							//brapa data yang ditampilkan di tiap halaman
							$baris_per_page = 8;

							if(isset($_GET['page']))
							{
								$page = $_GET['page'];
							}
							else
							{
								$page = 1;
							}

							//set data yang ditampilkan mulai data ke brapa 
							$start_from = ($page-1) * $baris_per_page;

							$sql = mysqli_query($mycon, "SELECT * FROM lelang where hapuskah = '0' LIMIT " .$start_from. "," .$baris_per_page);

							while($row = mysqli_fetch_array($sql))
							{
								echo '<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<a href="product-details-lelang.php?id=' .$row['id']. '"><h4><font color="#0000">' .$row['namabarang']. '</h4></a>
												<a class="link" href="product-details-lelang.php?id=' .$row['id']. '"><img src="../bismillah/images/' .$row['gambarbarang']. '" alt="" /></a>
												
												<h5>Harga Awal : Rp. ' .number_format($row['hargaawal'], 0, ',', '.').',-'  .'</h5>
												Lelang Berakhir<h6>' .$row['tanggalakhir']. '</h6> ';
											 if($row['statusmenang'] == '1')
                                                {
                                                echo '
												<h6>Dimenangkan Oleh</h6><h6>' .$row['namapenawar']. '</h6><h6> Harga : Rp. ' .number_format($row['hargatertinggi'], 0, ',', '.').',-'  .'</h6>';
												}
												echo '
												<input type="hidden" id="idlel" value="' .$row['id']. '" /> 
												<h4><font color="#00000"><span class="countdown" value="' .$row['tanggalakhir']. '"></span></h4>
												';
												// <a href="cart_proc.php?itemID=' .$row['id_barang']. '" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											
									  echo 	'</div>
										</div>
									</div>
								</div>';
							}
							$total_hlmn = 0;
							$tot_baris = 0;

							$sql1 = mysqli_query($mycon, "SELECT COUNT(id) as total_baris FROM lelang where hapuskah = '0'");
							while($row1 = mysqli_fetch_array($sql1))
							{
								$tot_baris = $row1['total_baris'];
							}
							$total_hlmn = ceil($tot_baris / $baris_per_page);
						?>
						
					</div><!--features_items-->
					<div class="center">
						<ul class="pagination">
						<?php
							for ($i=1; $i <=$total_hlmn; $i++) { 
								if($i == $page)
								{
									echo '<li><a class="active" href="lelang.php?page=' .$i. '">' .$i. '</a></li>';
								}
								else
								{
									echo '<li><a href="lelang.php?page=' .$i. '">' .$i. '</a></li>';
									//echo '&nbsp';
								}
							}
						echo '</ul>';
						?>	



					</div>	
					<br></br>
				</div>

			</div>
		</div>
	</section><!--/form-->
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jerpretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>

	<!-- script multiple countdown timer -->
	<script src="js/jquery.countdown.min.js"></script>
	<!-- <script src="jquery-1.4.js"></script> -->


	<!-- ******** SCRIPT COUNT DOWN TIMER********* -->
	<script>
	
	$(function(){
	  $('.countdown').each(function(){
	    $(this).countdown($(this).attr('value'), function(event) {
	      if (event.strftime('%D') == 0) 
	      {
	        if (event.strftime('%H') == 0) 
	        {
	          if (event.strftime('%M') == 0) 
	          {
	            if (event.strftime('%S') == 0) 
	            {
	            	var id = document.getElementById('idlel').value;
	            	$.ajax({
	            		type: "POST",
	            		url: "update_lelang.php",
	            		data: 'id=' +id,
	            		success: function (html){
	            			$(this).text('EXPIRED');
	            		}
	            	});
	            }
	            else
	            {
	            	// var id = document.getElementById('idlel').value;
	            	// $(this).text(id);
	              $(this).text(event.strftime('%S detik'));
	            }
	          }
	          else
	          {
	            $(this).text(event.strftime('%M menit %S detik'));
	          }
	        }
	        else
	        {
	          $(this).text(event.strftime('%H jam %M menit %S detik'));
	        }
	      }
	      else
	      {
	        $(this).text(event.strftime('%D hari %H jam %M menit %S detik'));
	      }
	    });
	  });
	});
	</script>
	<!-- ******** END OF SCRIPT COUNT DOWN TIMER********* -->

</body>
</html>





