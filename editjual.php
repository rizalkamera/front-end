<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
$qty = 0;
	
	//langsung tembak id pel 4
	$id_pel = 4;

	$id_nota = 0;
	 $jml = 0;
	 $bar = 0;
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
	//cek apakah pelanggan punya transaksi yang statusnya menunggu bukti transfer
	$f = mysqli_query($mycon, "SELECT * FROM postinganjualkamera n, user p WHERE n.iduser = p.id AND p.username = '" .$pengguna. "'");

	$res = mysqli_num_rows($f);
	//jika tidak ada, maka akan dialihkan ke beranda
	// if ($res < 1) {
	// 	echo '<script language="javascript">';
	//     echo 'document.location.href="index.php"';
	//     echo '</script>';
	// }
}
 ?>
 <?php
if(isset($_GET['id']))
{
    $ids = $_GET['id'];
}

?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pembayaran | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	 <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h1><font color="#0000">KURNIA </font><font color="#FE980F">KAMERA</font></h1></a>
						</div>	
					</div>
					 <div class="col-sm-5 col-sm-offset-3">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav">
                                <li class="dropdown"><a href="iklanku.php"><i class='far fa-list-alt' style="color:orange "></i></i>Iklan Saya</i></a>
                                    
                                </li>
                                <li class="dropdown"><a href="postpostingan.php"></i><i class='fa fa-camera' style="color:orange "></i> Jual </a></li>
                                <?php 
                                if(isset($_SESSION['aktif']))
                                {
                        echo '  <li class="dropdown"><a href=""><i class="fa fa-user "style="color:orange"></i> ' .$pengguna. '</a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="logout.php">
                                            <div class="mark">Logout</div>
                                        </a></li>  
                                    </ul>
                                </li>';
                                }
                                else if(empty($_SESSION['aktif']))
                                {
                        echo '  <li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
        
	</header><!--/header-->
	
	<section id="pembayaran"><!--pembayaran-->
		
			<div class="row">
				<div class="col-sm-7 col-sm-offset-2">
					<div class="bayar"><!--login form-->
						<center><h3>RUBAH PEJUALAN ANDA</h3></center></br></br>
						
                        <?php 
                      	
						  $query = "SELECT * FROM postinganjualkamera  WHERE id ='".$ids."'";
						  $sql = mysqli_query($mycon, $query);  // Eksekusi/Jalankan query dari variabel $query
						  $row = mysqli_fetch_array($sql); // Ambil data dari hasil eksekusi $sql

						                        
                        ?>
            
						 <form method="post" action="proseseditjual.php?ids=<?php echo $ids; ?>" enctype="multipart/form-data">
						 	
                            <div class="row"> Nama Produk 
                            <fieldset class="form-group col-sm-20">
                               <input type="text" name="namaproduk" class="form-control" value="<?php echo $row['namaproduk']; ?>"></br>
                        <div class="row">
                            <fieldset class="form-group col-xs-6"> Kondisi Barang 
                                <select class="form-control"  name="kondisi"  required>
                                   <option value="Baru" <?php if($row['kondisikamera'] == 'Baru'){ echo 'selected'; } ?>>Baru</option>
                                   <option value="Bekas" <?php if($row['kondisikamera'] == 'Bekas'){ echo 'selected'; } ?>>Bekas</option>
                                </select>
                            </fieldset>
                        </div>

						<div class="row">
                            <fieldset class="form-group col-xs-6"> Kategori
                                <select class="form-control"  name="kategori"  required>
                                   <option value="Kamera" <?php if($row['kategori'] == 'Kamera'){ echo 'selected'; } ?>>Kamera</option>
                                   <option value="Lensa" <?php if($row['kategori'] == 'Lensa'){ echo 'selected'; } ?>>Lensa</option>
                                   <option value="Asessoris" <?php if($row['kategori'] == 'Asessoris'){ echo 'selected'; } ?>>Asessoris</option>
                                </select>
                            </fieldset>
                        </div>
                        
                        <div class="row">
                            <fieldset class="form-group col-sm-10"> Deskripsi
                            	
                               <textarea type="text" class="form-control" name="deskripsi" cols="20" rows="10"><?php echo $row['deskripsi']; ?>
                                </textarea>	
                            </fieldset>
                        </div> 
                         <div class="row">
                            <fieldset class="form-group col-sm-4"> Harga Jual
                                <input type="number" class="form-control" name="hargajual" value="<?php echo $row['hargajual']; ?>">
                            </fieldset>
                        </div>

                         <div class="row">
                                    <fieldset class="form-group col-xs-9">
                                        <label for="gambarB">Gambar Kamera:</label> </br>
                                        <label for="file" class="file">
                                            <input type="file" name="foto"  accept="image/*" onchange="loadFile(event)"/>
                                            <br>
                                            <img id="output" style="width: 30%; height: 30%;" />
                                        </label>
                                        <img style="width: 20%; height: 20%;" src="../bismillah/images/jual/<?php echo $row['foto']; ?> "  /> 

                                         <label for="file" class="file">
                                            <input type="file" name="foto2"  accept="image/*" onchange="loadFile2(event)"/>
                                            <br>
                                            <img id="output2" style="width: 30%; height: 30%;" />
                                        </label>
                                        <img style="width: 20%; height: 20%;" src="../bismillah/images/jual/<?php echo $row['foto2']; ?> "  /> 

                                    </fieldset>
                                </div>
                                 <div class="row">
                                    <fieldset class="form-group col-xs-5">
                                        <input type="submit" class="btn btn-info" name="add" value="Tambahkan">
                                    </fieldset>
                            </div>     
                     </div>
				</div>
			</div>
		</div>
		<a href="jual.php"<a/><input type="submit" class"btn btn-fefault" style="background: #FE980F; border: 0 none; border-radius: 20; color: #FFFFFF; font-size: 20px; margin-bottom: 10px; margin-left: 20px; width: 10%;"  name="sub" value=" << Kembali" />
	</section><!--/form-->

	<script type="text/javascript">
		var loadFile = function(event) {
		    var output = document.getElementById('output');
		    output.src = URL.createObjectURL(event.target.files[0]);
	  	};
	  	var loadFile2 = function(event) {
		    var output2 = document.getElementById('output2');
		    output2.src = URL.createObjectURL(event.target.files[0]);
	  	};
	</script>
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretanya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>



