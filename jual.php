<?php 
include 'koneksi.php';
session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<?php 
include 'koneksi.php';

if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
}


?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- tambah script ini buat muculkan icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    
	<style>
		/*div untuk seting background color font logout*/
		.mark {
			background-color: #68686B;
		}
	</style>
</head><!--/head-->
<body>
	<header id="header"><!--header-->		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
			<a href="index.php"><h2><font color="#00000">JUAL </font><font color="#F88017">BELI</font> <font color="#00000">KAMERA</font></h2><H4> </font><font color="#0000">PUSATNYA NGEDEAL !!!!</font></H4></a>

						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href="iklanku.php"><i class='far fa-list-alt' style="color:orange "></i></i>Iklan Saya</i></a>
									
					          	</li>
								<li class="dropdown"><a href="postpostingan.php"></i><i class='fa fa-camera' style="color:orange "></i> Jual </a></li>
								<?php 
								if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user "style="color:orange"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Beranda</a></li>
								<li class="dropdown"><a href="">Kamera<i class="fa fa-angle-down"></i></a>
						            <ul role="menu" class="sub-menu">
						                <li><a href="dslr.php">DSLR</a></li>
										<li><a href="mirorrles.php">MIRORRLES</a>
										<li><a href="index.php?kat=3">ACTION CAM</a>
										</li> 
						            </ul>
						        </li> 
						        <li><a href="lensa.php">Lensa</a></li>
						        <li><a href="asessoris.php">Asessoris</a></li>
								<li><a href="lelang.php">Lelang </a></li>
								<li><a href="jual.php">Jual</a></li>
								<li><a href="carasewa.php">Cara Sewa</a></li>
								<li><a href="contact.php">Kontak</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	

	
	<section><!--SIDEBAR-->
		<div class="container">
			
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						
						</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Item yang Tersedia</h2>
						<?php
							include 'koneksi.php';


							//brapa data yang ditampilkan di tiap halaman
							$baris_per_page = 6;

							if(isset($_GET['page']))
							{
								$page = $_GET['page'];
							}
							else
							{
								$page = 1;
							}

							//set data yang ditampilkan mulai data ke brapa 
							$start_from = ($page-1) * $baris_per_page;

							$kat = "";
							if(isset($_GET['kat']))
							{
								$kat = $_GET['kat'];
							}
							if($kat == "")
							{
								$sql = mysqli_query($mycon, "SELECT * FROM `postinganjualkamera` WHERE  status = 'ditampilkan' AND hapuskah = 0 LIMIT " .$start_from. "," .$baris_per_page);
							

							}
							
							while($row = mysqli_fetch_array($sql))
							{
								echo '<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">';
													echo '<a href="product-details-jual.php?itemID=' .$row['id']. '"><h4><font color="#0000">' .$row['namaproduk']. ' </h2></a>'; 
												
													echo ' <a href="product-details-jual.php?itemID=' .$row['id']. '"><img src="../bismillah/images/jual/' .$row['foto']. '" alt="" /></a>';
													echo '<h4><font color="#F88017">Rp. ' .number_format($row['hargajual'], 0, ',', '.').',-'  .'</h4>';


												//pegecekan apakah stok tersedia atau tidak
														echo '<a href="product-details-jual.php?itemID=' .$row['id']. '" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart" style = "color: orange"></i>Info Detail</a>';
													
									  echo '</div>';
									  
										echo '
										</div>
									</div>
								</div>';
							}
							$total_hlmn = 0;
							$tot_baris = 0;

							//pembedaan query untuk semua barang dan barang kategori tertentu
							if($kat == "")
							{
								$sql1 = mysqli_query($mycon, "SELECT COUNT(id) as total_baris FROM postinganjualkamera where hapuskah = '0'");
							}
							else
							{
								$sql1 = mysqli_query($mycon, "SELECT COUNT(id) as total_baris from postinganjualkamera WHERE kategori = '" .$kat. "' AND hapuskah = '0'");
							}

							while($row1 = mysqli_fetch_array($sql1))
							{
								$tot_baris = $row1['total_baris'];
							}
							$total_hlmn = ceil($tot_baris / $baris_per_page);
						?>
						
					</div><!--features_items-->
					<div class="center">
						<ul class="pagination">
						<?php

						//pembedaan query untuk semua barang dan barang kategori tertentu
						if($kat == "")
						{
							for ($i=1; $i <=$total_hlmn; $i++) { 
								if($i == $page)
								{
									echo '<li><a class="active" href="jual.php?page=' .$i. '">' .$i. '</a></li>';
								}
								else
								{
									echo '<li><a href="jual.php?page=' .$i. '">' .$i. '</a></li>';
									//echo '&nbsp';
								}
							}
						}
						else
						{
							for ($i=1; $i <=$total_hlmn; $i++) { 
								if($i == $page)
								{
									echo '<li><a class="active" href="jual.php?kat=' .$kat. '&page=' .$i. '">' .$i. '</a></li>';
								}
								else
								{
									echo '<li><a href="jual.php?kat=' .$kat. '&page=' .$i. '">' .$i. '</a></li>';
									//echo '&nbsp';
								}
							}
						}
						
						echo '</ul>';
						?>	
					</div>	
					<br></br>
				</div>
			</div>
		</div>
	</section>
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-5 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera Probolinggo</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">sewa kamera semudah jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>


