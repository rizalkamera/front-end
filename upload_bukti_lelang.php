<?php


//silahkan upload foto bukti transfer anda.

//$_files

?>

<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
} ?>

<?php include 'header.php' ?>
<?php 
	if(isset($_SESSION['aktif']))
	{
		echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
				<ul role="menu" class="sub-menu">
			    <li><a href="logout.php">
			     <div class="mark">Logout</div>
			      </a></li>  
			      </ul>
			       </li>';
					}
					else if(empty($_SESSION['aktif']))
					{
					echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
		
	</header><!--/header-->
	
	<section id="pembayaran"><!--pembayaran-->
		<div class="container">
			<div class="step">
				<ul class="steps">
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pembayaran</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>2. Konfirmasi Pembayaran</li>
				  <li class="select">3. Upload Bukti Pembayaran</li>
				  <li >4. Selesai</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-sm-7 col-sm-offset-2">
					<div class="bayar"><!--login form-->
						<center><h3>Data Transfer</h3></center>
						<center>Silahkan isi data transfer yang telah anda lakukan di form berikut.</center><br>
						<form method="POST" action="upload_bukti_lelang.php" enctype="multipart/form-data">

						<div class="row">
                            <fieldset class="form-group col-xs-6">
                                <select class="form-control" name="bank" required>
                                	<option value="">--Bank Tujuan Transfer--</option>
                                	<?php
                                	if(isset($_POST['tuku']))
									{
										$bank = $_POST['bank'];
									}

									$t = mysqli_query($mycon, "SELECT * FROM bank WHERE hapuskah = '0'");
									while($res_t = mysqli_fetch_array($t))
									{
										if($bank == $res_t['id'])
										{
											echo '<option value="' .$res_t['id']. '" selected >Bank ' .$res_t['namabank']. ' </option>';
										}
										else
										{
											echo '<option value="' .$res_t['id']. '" >Bank ' .$res_t['namabank']. ' </option>';
										}
										
									} ?>
								</select>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-4">
                            	<input type="number" name="norek" class="form-control" placeholder="Nomor Rekening" required>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<input type="text" name="nama" class="form-control" placeholder="Atas Nama" required>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<div class="col-sm-2">
                            		Rp.
                            	</div>
                            	<div class="col-sm-10">
                            		<input type="number" name="nominal" class="form-control" placeholder="Nominal Transfer" required>
                            	</div>
                            	 
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<input type="file" id="ufile" name="ufile" class="form-control" accept="image/jpeg" onchange="loadFile(event)" required/>
                            </fieldset>
                        </div>
                        
                        <img id="output" style="width: 50%; height: 50%;" />
						
						<div class="col-sm-4 col-sm-offset-5">
							<div class="btn_bukti">
								<button type="submit" name="submit">Upload</button>
								</form>
							</div>
						</div>

						<!-- <input type="file" name="ufile" accept="image/jpeg"/>
						<div class="col-sm-4 col-sm-offset-5">
							<div class="btn_bukti">
								<button type="submit" name="submit">Upload</button>
								</form>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</section><!--/form-->

	<script type="text/javascript">
		var loadFile = function(event) {
		    var output = document.getElementById('output');
		    output.src = URL.createObjectURL(event.target.files[0]);
	  	};
	</script>
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<?php 

include 'koneksi.php';

if(isset($_POST['submit']))
{
     $bank1 = $_POST['bank'];
    $norek1 = $_POST['norek'];
    $nama1 = $_POST['nama'];
    $nominal1 = $_POST['nominal'];
	//cek apakah user sudah mengupload foto buktiya 
	if(!file_exists($_FILES['ufile']['tmp_name']))
	{
		echo '<script language="javascript">';
		echo 'alert("Belum ada file yang anda upload. Silahkan upload ulang foto bukti transfer anda.")';
		echo '</script>';
	}
	else
	{
		//simpan tipe" file yang diizinkan dalam atu array
		$allowed =  array('jpeg', 'jpg', 'JPG', 'JPEG');

		//simpan nama filenya
		$filename = $_FILES['ufile']['name'];

		//ambil format file dari $filename dengan method berikut
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		//cek apakah value $ext ada di dalam array $allowed
		if(!in_array($ext,$allowed) ) {
		    echo '<script language="javascript">'. 
			'window.alert("Maaf, format file tidak sesuai. Format file yang diminta adalah format JPG atau JPEG.")'.
			'</script>';
		}

		else
		{
			//lakukan split untuk ngambil nama file
			// $arr = explode('.',$_FILES['ufiles']['name']);
				// explode('.',$_FILES['files']['name'])
			// $arr1 = md5($_FILES['ufile']['name']);
			// $split = substr($arr1,0,10);

			// $name = $split . ".jpg";


			//sebelum set nama file yang baru, pisahkan nama file dan format file
	        $sp = explode(".",$_FILES['ufile']['name']);

	        // set nama file baru dengan melakukan enkripsi md5 dari nama file beserta formatnya concate waktu sistem
	        $nam = md5($_FILES['ufile']['name'] . time());

	        //substring hasil md5 dan concat dgn format file
	        $spl0 = substr($nam,0,10);
	        $spl = $spl0 . "." .$sp[count($sp) - 1];


			//simpan file kedalam database daan ke dalam project
			if(move_uploaded_file($_FILES['ufile']['tmp_name'], "images/upload_bukti/" .$spl))
			{
				
				//update nama file di database dan data transfer 

                //1. ambil id nota yg diupdate
				$s = mysqli_query($mycon, "SELECT l.id_lelang FROM lelang_resep l, pelanggan p WHERE l.pelanggan_id = p.id_pel AND p.username = '" .$pengguna. "' AND l.status_pembayaran = 'menunggu bukti transfer' AND p.hapuskah = '0' AND l.hapuskah = '0'");
				while ($res = mysqli_fetch_array($s)) 
				{
					$id_nota = $res['id_lelang'];
				}

                //2. lakukan update nama file jpg dan data transfer yang lainnya
				$sql = mysqli_query($mycon, "UPDATE lelang_resep SET bukti_transfer = '" .$spl. "', bank_id = '" .$bank1. "', no_rek = '" .$norek1. "', nama_pemilik = '" .$nama1. "', nom_trf = '" .$nominal1. "' WHERE id_lelang= '" .$id_nota. "'");

				echo '<script language="javascript">'. 
				'window.alert("File telah berhasil diupload.");'.
				'document.location.href="on_process.php"'.
				'</script>';
			}
		}

	}
}


?>



<!-- *********nama file problem********** -->