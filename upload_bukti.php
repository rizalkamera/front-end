<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
$qty = 0;
	
	//langsung tembak id pel 4
	$id_pel = 0;

	$id_nota = 0;
	 $jml = 0;
	 $bar = 0;
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
	//cek apakah pelanggan punya transaksi yang statusnya menunggu bukti transfer
	$f = mysqli_query($mycon, "SELECT * FROM user p, notasewa n
	                           WHERE p.id = n.user_id AND p.username = '".$pengguna."' AND p.hapuskah = '0' AND  n.hapuskah = '0'");

	$res = mysqli_num_rows($f);
	//jika tidak ada, maka akan dialihkan ke beranda
	if ($res < 1) {
		echo '<script language="javascript">';
	    echo 'document.location.href="index.php"';
	    echo '</script>';
	}
if(isset($_POST['tuku']))
{
	$nominalbayar = $_POST['nominal_bayar'];
}
//}
 ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pembayaran | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head><!--/head-->

<body>
	<?php include 'header.php' ?>

	
	<section id="pembayaran"><!--pembayaran-->
		<div class="container">
			<div class="step">
				<ul class="steps">
				  <ul class="steps">
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pimilihan Durasi Sewa</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>2.  Mengisi Form Data Sewa</li>
				  <li class="select" ><i class="fa fa-check-square-o"></i>3. Pilih Pembayaran</li>
				  <li >4. Upload Bukti Pembayaran</li>
				  <li >5. Selesai</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-sm-7 col-sm-offset-2">
					<div class="bayar"><!--login form-->
						<center><h3>Data Transfer</h3></center>
						<center>Silahkan isi data transfer di bawah ini.</center><br>
						<form method="POST" action="upload_bukti.php" enctype="multipart/form-data">
						<div class="row">
                            <fieldset class="form-group col-xs-6">
                                <select class="form-control" name="bank" required>
                                	<option value="">--Bank Tujuan Transfer--</option>
                                	<?php
									$t = mysqli_query($mycon, "SELECT * FROM bank WHERE hapuskah = '0'");
									while($res_t = mysqli_fetch_array($t))
									{
										echo '<option value="' .$res_t['idbank']. '" >Bank ' .$res_t['namabank']. ' </option>';			
									} ?>
								</select>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-4">
                            	<input type="number" name="norek" class="form-control" placeholder="Nomor Rekening" required>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<input type="text" name="nama" class="form-control" placeholder="Atas Nama" required>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<div class="col-sm-2">
                            		Rp.
                            	</div>
                            	<div class="col-sm-10">
                            		<input type="number" name="nominal" class="form-control" value="<?php echo $nominalbayar; ?>" readonly />
                            	</div>
                            	 
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<input type="file" id="ufile" name="ufile" class="form-control" accept="image/jpeg" onchange="loadFile(event)" required/>
                            </fieldset>
                        </div>
                        
                        <img id="output" style="width: 50%; height: 50%;" />
						
						<div class="col-sm-4 col-sm-offset-5">
							<div class="btn_bukti">
								<button type="submit" name="submit">Upload</button>
								</form>
							</div>
						</div>

						<!-- <input type="file" name="ufile" accept="image/jpeg"/>
						<div class="col-sm-4 col-sm-offset-5">
							<div class="btn_bukti">
								<button type="submit" name="submit">Upload</button>
								</form>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</section><!--/form-->

	<script type="text/javascript">
		var loadFile = function(event) {
		    var output = document.getElementById('output');
		    output.src = URL.createObjectURL(event.target.files[0]);
	  	};
	</script>
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamere</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<?php 

include 'koneksi.php';

if(isset($_POST['submit']))
{
    $bank1 = $_POST['bank'];
    $norek1 = $_POST['norek'];
    $nama1 = $_POST['nama'];
    $nominal1 = $_POST['nominal'];

    // echo '<script language="javascript">';
    // echo 'window.alert("bank='.$bank1.'");';
    // // echo 'window.alert("transaksi baru, beli brg dari index.");';
    // // echo 'document.location.href="index.php"';
    // echo '</script>';

    // echo '<script language="javascript">';
    // echo 'window.alert("norek='.$norek1.'");';
    // // echo 'window.alert("transaksi baru, beli brg dari index.");';
    // // echo 'document.location.href="index.php"';
    // echo '</script>';

    // echo '<script language="javascript">';
    // echo 'window.alert("nama='.$nama1.'");';
    // // echo 'window.alert("transaksi baru, beli brg dari index.");';
    // // echo 'document.location.href="index.php"';
    // echo '</script>';

    // echo '<script language="javascript">';
    // echo 'window.alert("nominal='.$nominal1.'");';
    // // echo 'window.alert("transaksi baru, beli brg dari index.");';
    // // echo 'document.location.href="index.php"';
    // echo '</script>';

	//cek apakah user sudah mengupload foto buktiya 
	if(!file_exists($_FILES['ufile']['tmp_name']))
	{
		echo '<script language="javascript">';
		echo 'alert("Belum ada file yang anda upload. Silahkan upload ulang foto bukti transfer anda.")';
		echo '</script>';
	}
	else
	{
		//simpan tipe" file yang diizinkan dalam atu array
		$allowed =  array('jpeg', 'jpg', 'JPG', 'JPEG');

		//simpan nama filenya
		$filename = $_FILES['ufile']['name'];

		//ambil format file dari $filename dengan method berikut
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		//cek apakah value $ext ada di dalam array $allowed
		if(!in_array($ext,$allowed) ) {
		    echo '<script language="javascript">'. 
			'window.alert("Maaf, format file tidak sesuai. Format file yang diminta adalah format JPG atau JPEG.")'.
			'</script>';
		}

		else
		{
			//lakukan split untuk ngambil nama file
			// $arr = explode('.',$_FILES['ufiles']['name']);
				// explode('.',$_FILES['files']['name'])
			// $arr1 = md5($_FILES['ufile']['name']);
			// $split = substr($arr1,0,10);

			// $name = $split . ".jpg";


			//sebelum set nama file yang baru, pisahkan nama file dan format file
	        $sp = explode(".",$_FILES['ufile']['name']);

	        // set nama file baru dengan melakukan enkripsi md5 dari nama file beserta formatnya concate waktu sistem
	        $nam = md5($_FILES['ufile']['name'] . time());

	        //substring hasil md5 dan concat dgn format file
	        $spl0 = substr($nam,0,10);
	        $spl = $spl0 . "." .$sp[count($sp) - 1];


			//simpan file kedalam database daan ke dalam project
			if(move_uploaded_file($_FILES['ufile']['tmp_name'], "../bismillah/images/upload_bukti/" .$spl))
			{
				
				//update nama file di database dan data transfer 

                //1. ambil id nota yg diupdate
             	$sw = mysqli_query($mycon,"SELECT id FROM notasewa WHERE hapuskah = '0' order by id desc limit 1");
				while($sq = mysqli_fetch_array($sw))
				{
					$bar = $sq['id'];
				}
				
                //2. lakukan update nama file jpg dan data transfer yang lainnya
				$e = mysqli_query($mycon, "UPDATE notasewa set norekening = '$norek1', namapemilik = '$nama1', nominaltransfer = '$nominal1', buktitransfer = '$spl', bankid = '$bank1', statusnota = 'menunggu validasi pembayaran' where id = '$bar'");
			    if(!$e)
                {
                    echo 'error ' .mysqli_error($mycon);
                }
                else
                {
					echo '<script language="javascript">'. 
					'window.alert("Data transfer telah disimpan.");'.
					'document.location.href="on_processpesan.php"'.
					'</script>';
				}
			}

		}
	}

}

}

?>



<!-- *********nama file problem********** -->