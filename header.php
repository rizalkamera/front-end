<header id="header"><!--header-->		
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="index.php"><h1><font color="#00000">KURNIA </font><font color="#F88017">KAMERA</font></h1></a>
                    </div>	
                </div>
                <div class="col-sm-5 col-sm-offset-3">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href=""><i class="fa fa-money"style="font-size:19px;color:orange"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="checkout.php">
                                        <div class="mark">Pemesanan Kamera</div>
                                    </a></li>
                                    <li><a href="histori.php">
                                        <div class="mark">Histori Pemesanan</div>
                                    </a></li>
                                    <li><a href="checkout_lelang.php">
                                        <div class="mark">Transaksi Lelang</div>
                                    </a></li> 
                                </ul>
                            </li>
                            <?php
                            $counter = null;
                            if(isset($_SESSION['id']))
                            { 
                                $sql = mysqli_query($mycon, "SELECT sum(jmlsewa) as cart FROM hub_notasewa_dan_kamera WHERE nota_id in (SELECT id FROM notasewa WHERE user_id = " . $_SESSION['id'] . ") AND keranjang = 1");
                                $row = mysqli_fetch_assoc($sql);
                                $counter = $row['cart'];
                            }
                            ?>
                            <li class="dropdown"><a href="cart.php"><?= ($counter) ? $counter : '' ?><i class="fa fa-shopping-cart"  style="font-size:19px;color:orange"></i> Keranjang</a></li>
                            <?php 
                            if(isset($_SESSION['aktif']))
                            {
                    echo '	<li class="dropdown"><a href=""><i class="fa fa-user"style="font-size:19px;color:orange"></i> ' .$pengguna. '</a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="logout.php">
                                        <div class="mark">Logout</div>
                                    </a></li>  
                                </ul>
                            </li>';
                            }
                            else if(empty($_SESSION['aktif']))
                            {
                    echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="index.php" class="active">Beranda</a></li>
                            <li class="dropdown"><a href="">Kamera<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="dslr.php">DSLR</a></li>
                                    <li><a href="mirorrles.php">MIRORRLES</a>
                                    <li><a href="actioncam.php">ACTION CAM</a>
                                    </li> 
                                </ul>
                            </li> 
                            <li><a href="lensa.php">Lensa</a></li>
                            <li><a href="asessoris.php">Asessoris</a></li>
                            <li><a href="lelang.php">Lelang </a></li>
                            <li><a href="jual.php">Jual</a></li>
                            <li><a href="carasewa.php">Cara Sewa</a></li>   
                            <li><a href="contact.php">Tentang</a></li>
                            <li><a href ="" data-toggle="modal" data-target="#SPK">Bantuan</a></li>
                        </ul>
                            
                        <!-- Modal -->
                        <div class="modal fade" id="SPK" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Kami Akan Membantu Kebutuhan Anda</h4>
                              </div>
                              <div class="modal-body">
                                <form class="form-horizontal" action="spk.php" method="POST">
                                <div class="form-group">
                                    <label for="">Untuk apa Anda menggunakan kamera tersebut?</label>
                                    <select class="form-control" name="purpose" required>
                                      <option value="fotografi">Fotografi</option>
                                      <option value="videografi">Videografi</option>
                                    </select>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="">Dimana Anda akan menggunakan kamera?</label>
                                    <select class="form-control" name="environment" required>
                                      <option value="indoor">Indoor</option>
                                      <option value="outdoor">Outdoor</option>
                                      <option value="inout">Indoor & Outdoor</option>
                                    </select>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="">Berapa budget sewa yang Anda miliki?</label>
                                    <select class="form-control" name="budget" required>
                                     
                                      <option value="80000">40000 - 80000</option>
                                      <option value="120000">80000 - 120000</option>
                                      <option value="200000">120000 - 200000</option>
                                    </select>
                                </div>
                                 <br>
                                <div class="form-group">
                                    <label for=""> Fitur manakah yang Anda inginkan?</label>
                                    <select class="form-control" name="feature" required>
                                      <option value="jarak jauh">Mengambil objek dari jarak jauh</option>
                                      <option value="jarak dekat">Mengambil objek dari jarak dekat</option>
                                      <option value="panorama">Panorama (pemandangan)</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Apakah Anda butuh aksesoris tambahan?</label>
                                    <select class="form-control" name="extras" required>
                                      <option value="ya">Ya</option>
                                      <option value="tidak">Tidak</option>
                                    </select>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" name="proses" class="btn btn-success">Proses</button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
</header><!--/header-->