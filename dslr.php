<?php 
include 'koneksi.php';
session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<?php 
include 'koneksi.php';

if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
}


?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- tambah script ini buat muculkan icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    
	<style>
		/*div untuk seting background color font logout*/
		.mark {
			background-color: #68686B;
		}
	</style>
</head><!--/head-->
<body>
	<?php include 'header.php' ?>

	
	<section><!--SIDEBAR-->
		<div class="container">
			
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Kategori</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="dslr.php">Canon</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="index.php?kat=2">Nikon</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Item yang Tersedia</h2>
						<?php
							include 'koneksi.php';


							//brapa data yang ditampilkan di tiap halaman
							$baris_per_page = 6;

							if(isset($_GET['page']))
							{
								$page = $_GET['page'];
							}
							else
							{
								$page = 1;
							}

							//set data yang ditampilkan mulai data ke brapa 
							$start_from = ($page-1) * $baris_per_page;

							$kat = "";
							if(isset($_GET['kat']))
							{
								$kat = $_GET['kat'];
							}
							if($kat == "")
							{
								$sql = mysqli_query($mycon, "SELECT * FROM kamera where kategori_id = '1' AND jeniskamera = '1' AND hapuskah = 0 order by kategori_id LIMIT " .$start_from. "," .$baris_per_page);
								//$sql = mysqli_query($mycon, "SELECT * FROM kamera where  hapuskah = 0 LIMIT " .$start_from. "," .$baris_per_page);
							}
							else
							{
								$sql = mysqli_query($mycon, "SELECT * FROM kamera where kategori_id = '" .$kat. "' AND hapuskah = 0 LIMIT " .$start_from. "," .$baris_per_page);
								//$sql = mysqli_query($mycon, "SELECT * FROM kamera where kategori = '" .$kat. "' AND hapuskah = 0 LIMIT " .$start_from. "," .$baris_per_page);
							}
							while($row = mysqli_fetch_array($sql))
							{
								echo '<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">';
												
												echo '<a href="product-details.php?itemID=' .$row['id']. '"><h4	><font color="#0000">' .$row['namakamera']. '</h4></a>'; 
												echo '<h5><font color="#F88017">Mulai Rp</font>. ' .number_format($row['harga_6jam'], 0, ',', '.').'<font color="#F88017"> /6 jam</font></h5>';
												echo '<a href="product-details.php?itemID=' .$row['id']. '"><img src="../bismillah/images/' .$row['gambar']. '" alt="" /></a>';
													
												
																
												
												//pegecekan apakah stok tersedia atau tidak
													if($row['stoktotal'] > 0) {
														echo '<a href="product-details.php?itemID=' .$row['id']. '" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart" style="font-size:19px;color:orange"></i>Sewa Produk</a>';
													}
													else {
														echo '<a href="#" class="btn btn-default add-to-cart" disabled><i class="fa fa-shopping-cart"></i>Stok Habis</a>'; }
												
									  echo '</div>';
									  
										echo '
										</div>
									</div>
								</div>';
							}
							$total_hlmn = 0;
							$tot_baris = 0;

							//pembedaan query untuk semua barang dan barang kategori tertentu
							if($kat == "")
							{
								$sql1 = mysqli_query($mycon, "SELECT COUNT(id) as total_baris FROM kamera where kategori_id = '1' AND jeniskamera = '1' AND hapuskah = 0 order by kategori_id LIMIT " .$start_from. "," .$baris_per_page);
							}
							else
							{
								$sql1 = mysqli_query($mycon, "SELECT COUNT(id) as total_baris FROM kamera where kategori_id = '" .$kat. "' AND hapuskah = 0 LIMIT " .$start_from. "," .$baris_per_page);
							}

							while($row1 = mysqli_fetch_array($sql1))
							{
								$tot_baris = $row1['total_baris'];
							}
							$total_hlmn = ceil($tot_baris / $baris_per_page);
						?>
						
					</div><!--features_items-->
					<div class="center">
						<ul class="pagination">
						<?php

						//pembedaan query untuk semua barang dan barang kategori tertentu
						if($kat == "")
						{
							for ($i=1; $i <=$total_hlmn; $i++) { 
								if($i == $page)
								{
									echo '<li><a class="active" href="dslr.php?page=' .$i. '">' .$i. '</a></li>';
								}
								else
								{
									echo '<li><a href="dslr.php?page=' .$i. '">' .$i. '</a></li>';
									//echo '&nbsp';
								}
							}
						}
						else
						{
							for ($i=1; $i <=$total_hlmn; $i++) { 
								if($i == $page)
								{
									echo '<li><a class="active" href="dslr.php?kat=' .$kat. '&page=' .$i. '">' .$i. '</a></li>';
								}
								else
								{
									echo '<li><a href="dslr.php?kat=' .$kat. '&page=' .$i. '">' .$i. '</a></li>';
									//echo '&nbsp';
								}
							}
						}
						
						echo '</ul>';
						?>	
					</div>	
					<br></br>
				</div>
			</div>
		</div>
	</section>
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-5 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera Probolinggo</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">sewa kamera semudah jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>


