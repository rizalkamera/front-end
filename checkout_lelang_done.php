<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
} ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pembayaran | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head><!--/head-->

<style>
	#bank-radio:hover {
    	color: white;
        background-color: #FE980F;
        border-radius: 7px;
    }
</style>

<body>
	
<?php include 'header_noMenuBar.php' ?>
	
	<section id="pembayaran"><!--pembayaran-->
		<div class="container">
			<div class="step">
				<ul class="steps">
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pembayaran</li>
				  <li class="select">2. Upload Bukti Pembayaran</li>
				  <li >3. Selesai</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-sm-9 col-sm-offset-2">
					<div class="bayar"><!--login form-->
						<p><font color="#DC143C">PERHATIAN :<br></p></font>
                    <p>Setelah anda melakukan transfer ke rekening di bawah ini, mohon simpanlah bukti transfer yang anda dapatkan. Karena sistem akan meminta anda untuk mengupload foto bukti transfer anda.</p>
						<div class="col-sm-11 col-sm-offset-1">
							<div class="detil_bayar">
								<!-- <p>Total transaksi yang harus anda bayar adalah</p>
								<strong><p style="margin-left: 20%"> -->
								<?php
								//$s = mysqli_query($mycon, "SELECT l.hargatertinggi FROM lelang l, user p WHERE l.pelanggan_id = p.id AND p.username = '" .$pengguna. "' AND l.statuspembayaran = 'menunggu bukti transfer' AND p.hapuskah = '0' AND l.hapuskah = '0'");
								// $s = mysqli_query($mycon, "SELECT l.hargatertinggi FROM lelang l, user p WHERE l.pelanggan_id = p.id AND p.username = '" .$pengguna. "' AND l.statuspembayaran = 'menunggu bukti transfer' AND p.hapuskah = '0' AND l.hapuskah = '0'");
        //                         if(!$s)
        //                         {
        //                             echo mysqli_error($mycon);
        //                         }
        //                         while ($w = mysqli_fetch_array($s)) {
								// 	$gt = $w['hargatertinggi'];
								// }
								// echo 'Rp.' .number_format($gt, 0, ',', '.'). ',-' ?>

								</p></strong>
								<br>
						
	
						<h3>Data Transfer</h3>
						<h4>Silahkan isi data transfer yang telah anda lakukan di form berikut.</h4>
						<form method="POST" action="checkout_lelang_done.php" enctype="multipart/form-data">
						<div class="row">
                            <fieldset class="form-group col-xs-6">
                                <select class="form-control" name="bank" required>
                                	<option value="">--Bank Tujuan Transfer--</option>
                                	<?php
									$t = mysqli_query($mycon, "SELECT * FROM bank WHERE hapuskah = '0'");
									while($res_t = mysqli_fetch_array($t))
									{
										echo '<option value="' .$res_t['idbank']. '" >Bank ' .$res_t['namabank']. ' </option>';			
									} ?>
								</select>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-4">
                            	<input type="number" name="norek" class="form-control" placeholder="Nomor Rekening" required>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<input type="text" name="nama" class="form-control" placeholder="Atas Nama" required>
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<div class="col-sm-2">
                            		Rp.
                            	</div>

                                <?php
                               
                                $s = mysqli_query($mycon, "SELECT * FROM lelang");
                                if(!$s)
                                {
                                    echo mysqli_error($mycon);
                                }
                                while ($w = mysqli_fetch_array($s)) {
                                    $gt = $w['hargatertinggi'];
                                }
                               ?>

                            	<div class="col-sm-10">
                            		<input type="number" name="nominal" class="form-control" value="<?php echo $gt; ?>" readonly />
                            	</div>
                            	 
                            </fieldset>
                        </div>
                        <div class="row">
                            <fieldset class="form-group col-sm-5">
                            	<input type="file" id="ufile" name="ufile" class="form-control" accept="image/jpeg" onchange="loadFile(event)" required/>
                            </fieldset>
                      
                        
                        <img id="output" style="width: 20%; height: 20%;" />
						  </div>
						<div class="col-sm-2 col-sm-offset-2">
							<div class="btn_bukti">
								<button type="submit" name="submit">Upload</button>
								</form>
							</div>
						</div>

						<!-- <input type="file" name="ufile" accept="image/jpeg"/>
						<div class="col-sm-4 col-sm-offset-5">
							<div class="btn_bukti">
								<button type="submit" name="submit">Upload</button>
								</form>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</section><!--/form-->

	<script type="text/javascript">
		var loadFile = function(event) {
		    var output = document.getElementById('output');
		    output.src = URL.createObjectURL(event.target.files[0]);
	  	};
	</script>
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamere</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<?php 

include 'koneksi.php';


//set ukuran file maksimal
define ("MAX_SIZE","2000");

//method ambil format gambar
function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }

if(isset($_POST['submit']))
{
    $bank1 = $_POST['bank'];
    $norek1 = $_POST['norek'];
    $nama1 = $_POST['nama'];
    $nominal1 = $_POST['nominal'];


    if(!file_exists($_FILES['ufile']['tmp_name']))
    {
        echo '<script language="javascript">';
        echo 'alert("Belum ada file yang anda upload. Silahkan upload ulang foto bukti transfer anda.")';
        echo '</script>';
    }
    else
    {
        //simpan tipe" file yang diizinkan dalam atu array
        $allowed =  array('jpeg', 'jpg', 'JPG', 'JPEG');

        //simpan nama filenya
        $filename = $_FILES['ufile']['name'];

        //ambil format file dari $filename dengan method berikut
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        //cek apakah value $ext ada di dalam array $allowed
        if(!in_array($ext,$allowed) ) {
            echo '<script language="javascript">'. 
            'window.alert("Maaf, format file tidak sesuai. Format file yang diminta adalah format JPG atau JPEG.")'.
            '</script>';
        }

        else
        {
            //lakukan split untuk ngambil nama file
            // $arr = explode('.',$_FILES['ufiles']['name']);
                // explode('.',$_FILES['files']['name'])
            // $arr1 = md5($_FILES['ufile']['name']);
            // $split = substr($arr1,0,10);

            // $name = $split . ".jpg";


            //sebelum set nama file yang baru, pisahkan nama file dan format file
            $sp = explode(".",$_FILES['ufile']['name']);

            // set nama file baru dengan melakukan enkripsi md5 dari nama file beserta formatnya concate waktu sistem
            $nam = md5($_FILES['ufile']['name'] . time());

            //substring hasil md5 dan concat dgn format file
            $spl0 = substr($nam,0,10);
            $spl = $spl0 . "." .$sp[count($sp) - 1];


            //simpan file kedalam database daan ke dalam project
            if(move_uploaded_file($_FILES['ufile']['tmp_name'], "../bismillah/images/upload_bukti/" .$spl))
            {
                
                //update nama file di database dan data transfer 

                //1. ambil id nota yg diupdate
            

    //ambil id lelang yang mau diupdate
    $s = mysqli_query($mycon, "SELECT l.id FROM lelang l, user p WHERE l.pelanggan_id = p.id AND p.username = '" .$pengguna. "'");
    while ($res = mysqli_fetch_array($s)) 
    {
        $id_lelang = $res['id'];
    }
    
    //update email di lelang resep terkait dan status transaksi jadi menunggu bukti pembayaran
    // $sql1 = mysqli_query($mycon, "UPDATE lelang SET statuspembayaran = 'menunggu bukti transfer' WHERE id = '" .$id_lelang. "'");

    // if (!$sql1) {
    //     echo 'error ' .mysqli_error($mycon);
    // }


        $q = mysqli_query($mycon, "UPDATE lelang SET buktitransfer = '$spl', bankid = '$bank1', norek = '$norek1', namapemilik = '$nama1', nominaltransfer = '$nominal1', statuspembayaran = 'menunggu bukti transfer' WHERE id = '$id_lelang'");
			
				  
                 if(!$q)
                 {
                	echo 'error ' .mysqli_error($mycon);
             	 }
             	 else
             	 {
	               		echo '<script language="javascript">
	                    window.alert("Data berhasil diinput")
	                    document.location.href="on_process.php"
	                	</script>';
                  
                  }
			}
        }
        }
        }       
		
	?>
  