<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
// if (empty($_SESSION['aktif'])) {
//     echo '<script language="javascript">';
//     echo 'window.alert("Anda harus login terlebih dahulu!");';
//     echo 'document.location.href="login.php"';
//     echo '</script>';
// }
// else if(isset($_SESSION['aktif']))
// {
// 	$pengguna = $_SESSION['aktif'];
// 	//cek apakah pelanggan punya transaksi yang statusnya menunggu bukti transfer
// 	$f = mysqli_query($mycon, "SELECT * FROM nota_jual n, pelanggan p WHERE n.pelanggan_id = p.id_pel AND p.username = '" .$pengguna. "' AND n.status_pesanan = 'menunggu bukti transfer' AND n.hapuskah = '0' AND p.hapuskah = '0'");

// 	$res = mysqli_num_rows($f);
// 	//jika tidak ada, maka akan dialihkan ke beranda
// 	if ($res < 1) {
// 		echo '<script language="javascript">';
// 	    echo 'document.location.href="index.php"';
// 	    echo '</script>';
// 	}
// } ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pembayaran | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head><!--/head-->

<body>
	
<?php include 'header_noMenuBar.php' ?>
	
	<section id="pembayaran"><!--pembayaran-->
		<div class="container">
			<div class="step">
				<ul class="steps">
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pemilihan Durasi Sewa</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>2. Mengisi Form Data Sewa</li>
				  <li >3.Pilih Pembayaran </li>
				  <li >4.Upload Bukti Transfer </li>
				  <li >5. Selesai</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-sm-7 col-sm-offset-2">
					<div class="bayar"><!--login form-->
						<!-- <center><h3>Data Transfer</h3></center>
						<center>Silahkan isi data transfer yang telah anda lakukan di form berikut.</center><br>
						<form method="POST" action="upload_bukti.php" enctype="multipart/form-data">
						<div class="row">
                            <fieldset class="form-group col-xs-6">
                                <select class="form-control" name="bank" required>
                                	<option value=""></option> -->
                                	<?php
         //                       	if(isset($_POST['tuku']))
									// {
									// 	$bank = $_POST['bank'];
									// }

									// $t = mysqli_query($mycon, "SELECT * FROM bank WHERE hapuskah = '0'");
									// while($res_t = mysqli_fetch_array($t))
									// {
									// 	if($bank == $res_t['id'])
									// 	{
									// 		echo '<option value="' .$res_t['id']. '" selected >Bank ' .$res_t['nama_bank']. ' </option>';
									// 	}
									// 	else
									// 	{
									// 		echo '<option value="' .$res_t['id']. '" >Bank ' .$res_t['nama_bank']. ' </option>';
									// 	}
										
									 ?>
								</select>
                            </fieldset>
                        </div>
                        <form method="POST" action="datasewa.php">
                         <div class="row">
                                <div class="col-sm-6">
                                    <h3>Form Data Sewa</h3>
                                    <div class="row">
                                        <fieldset class="form-group">
                                            <label for="isiResep">Nama :</label>
                                                <input type='text' class="form-control" name="nama" placeholder="Nama" required/>
                                        </fieldset>
                                    </div>
                                    <div class="row">
                                        <fieldset class="form-group">
                                            <label for="isiResep">Alamat Lengkap :</label>
                                            <!-- <div class='input-group date' id='datetimepicker2'> -->
                                            <textarea class="form-control" name="alamat" cols="50" rows="3" required> </textarea>
                                           
                                            <!-- </div> -->
                                        </fieldset>
                                    </div>
                                     <div class="row">
                                        <fieldset class="form-group">
                                            <label>No Tlp :</label>
                                            <div class='input-group' >
                                                <input type='number' name="notlp" class="form-control" required/>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="row">
                                        <fieldset class="form-group">
                                           <!--  <div class="form-group col-xs-4"> -->
                                                <label>Email :</label>
                                                 <input type='text' class="form-control" name="email" required/>
                                                
                                            <!-- </div> -->
                                        </fieldset>
                                    </div>
                                    
                                  <!-- <form action="process.php?act=spnLelangBrg" method="post" class="form-center" role="form" enctype="multipart/form-data"> -->
                                    <div class="row">
                                        <fieldset class="form-group">
                                            <label for="isiResep">Waktu Mulai:</label>
                                            <div class='input-group date' >
                                            	<?php
                                            	$q = mysqli_query($mycon, "SELECT lamasewa FROM notasewa WHERE hapuskah = '0' order by id desc limit 1");
                                            	while($sq = mysqli_fetch_array($q))
												{
													$durasi = $sq['lamasewa'];
												}
                                            	?>
                                            	<input type ="hidden" name = 'durasi' id = 'durasi' value = "<?php echo $durasi;?>">
                                                <input type="datetime-local" class="form-control" id="wktuMulai" name='wktuMulai' required oninvalid="this.setCustomValidity('Harap Diisi')"/>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="row">
                                        <fieldset class="form-group">
                                            <label for="isiResep">Waktu Selesai:</label>
                                            <span id= "selesai" ></span>
                                           
                                        </fieldset>
                                    </div>

                                    
										
						<!-- <input type="file" name="ufile" accept="image/jpeg"/>
						<div class="col-sm-4 col-sm-offset-5">
							<div class="btn_bukti">
								<button type="submit" name="submit">Upload</button>
								</form>
							</div>
						</div> -->

						<div class="col-sm-4 col-sm-offset-5">
				<div class="btn">
					<button type="submit" name="simpn" >proses</button>
					</form>

					<!-- </form> -->
					<br></br>
					<br></br>
				</div>
			</div>
		</div>
	</section><!--/form-->

	<script type="text/javascript">
		var mymulai = document.getElementById('wktuMulai');
		var hargasewa2 = document.getElementById('selesai');
		var temp = '';
		var durasi1 = document.getElementById('durasi');
		Date.prototype.addHours= function(h){
    	this.setHours(this.getHours()+h);
   		 return this;
		}

		

		mymulai.onchange = function(){
			 alert(this.value);
			 alert(durasi1.value);
			  temp = new Date(this.value);
			 alert(temp);
			 	alert(TanggalIndo(temp));
			//document.getElementById("hargasewa1").innerHTML = formatRupiah(this.value, 'Rp. ');
			//new Date().toLocaleString('en-US', { timeZone: 'Asia/Jakarta' })

			var indoTime = new Date().toLocaleString({timeZone: 'Asia/Jakarta',format:'YYYY-MM-DD'});
			//var indoTime = new Date.format('YYYY-MM-DD');
			indoTime = new Date(indoTime).addHours(durasi1.value);
			var indoTime = new Date().addHours(durasi1.value);
			console.log('Indo time: '+indoTime);
			 document.getElementById("selesai").innerHTML = indoTime.toLocaleString();

		}
	</script>
	
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<?php 

include 'koneksi.php';

if(isset($_POST['simpn']))
{
		$nm = $_POST['nama'];
		$almt = $_POST['alamat'];
		$notlp= $_POST['notlp'];
		$email = $_POST['email'];
		$wMulai = $_POST['wktuMulai'];
        $wSelesai = $_POST['wktuSelesai'];
       
       //ambil id nota yang baru tadi
				$sw = mysqli_query($mycon,"SELECT id FROM notasewa WHERE hapuskah = '0' order by id desc limit 1");
				while($sq = mysqli_fetch_array($sw))
				{
					$bar = $sq['id'];
				}
					// echo $bar;
        $e = mysqli_query($mycon, "UPDATE notasewa set namapenyewa = '$nm', alamatpenyewa = '$almt', notlpn = '$notlp',tanggalsewa = '$wMulai' where id = '$bar'");

                    if(!$e)
                    {
                        echo 'error ' .mysqli_error($mycon);
                    }
                    else
                    {
                    	
                    	 echo '<script language="javascript"> 
                      	alert("Data Berhasil diinput.")
                      	document.location.href="checkout_done.php"
                      	</script>';
                    }
                    
                    
                }
            
if(isset($_POST['submit']))
{
    $bank1 = $_POST['bank'];
    $norek1 = $_POST['norek'];
    $nama1 = $_POST['nama'];
    $nominal1 = $_POST['nominal'];



	//cek apakah user sudah mengupload foto buktiya 
	if(!file_exists($_FILES['ufile']['tmp_name']))
	{
		echo '<script language="javascript">';
		echo 'alert("Belum ada file yang anda upload. Silahkan upload ulang foto bukti transfer anda.")';
		echo '</script>';
	}
	else
	{
		//simpan tipe" file yang diizinkan dalam atu array
		$allowed =  array('jpeg', 'jpg', 'JPG', 'JPEG');

		//simpan nama filenya
		$filename = $_FILES['ufile']['name'];

		//ambil format file dari $filename dengan method berikut
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		//cek apakah value $ext ada di dalam array $allowed
		if(!in_array($ext,$allowed) ) {
		    echo '<script language="javascript">'. 
			'window.alert("Maaf, format file tidak sesuai. Format file yang diminta adalah format JPG atau JPEG.")'.
			'</script>';
		}

		else
		{
			//lakukan split untuk ngambil nama file
			// $arr = explode('.',$_FILES['ufiles']['name']);
				// explode('.',$_FILES['files']['name'])
			// $arr1 = md5($_FILES['ufile']['name']);
			// $split = substr($arr1,0,10);

			// $name = $split . ".jpg";


			//sebelum set nama file yang baru, pisahkan nama file dan format file
	        $sp = explode(".",$_FILES['ufile']['name']);

	        // set nama file baru dengan melakukan enkripsi md5 dari nama file beserta formatnya concate waktu sistem
	        $nam = md5($_FILES['ufile']['name'] . time());

	        //substring hasil md5 dan concat dgn format file
	        $spl0 = substr($nam,0,10);
	        $spl = $spl0 . "." .$sp[count($sp) - 1];


			//simpan file kedalam database daan ke dalam project
			if(move_uploaded_file($_FILES['ufile']['tmp_name'], "images/upload_bukti/" .$spl))
			{
				
				//update nama file di database dan data transfer 

                //1. ambil id nota yg diupdate
				$s = mysqli_query($mycon, "SELECT n.id_notaJual FROM nota_jual n, pelanggan p WHERE n.pelanggan_id = p.id_pel AND p.username = '" .$pengguna. "' AND n.status_pesanan = 'menunggu bukti transfer' AND p.hapuskah = '0' AND n.hapuskah = '0'");
				while ($res = mysqli_fetch_array($s)) 
				{
					$id_nota = $res['id_notaJual'];
				}

                //2. lakukan update nama file jpg dan data transfer yang lainnya
				$sql = mysqli_query($mycon, "UPDATE nota_jual SET bukti_transfer = '" .$spl. "', bank_id = '" .$bank1. "', no_rek = '" .$norek1. "', nama_pemilik = '" .$nama1. "', nom_trf = '" .$nominal1. "' WHERE id_notaJual = '" .$id_nota. "'");

				echo '<script language="javascript">'. 
				'window.alert("Data transfer telah disimpan.");'.
				'document.location.href="on_process.php"'.
				'</script>';
			}
		}

	}
}


?>



<!-- *********nama file problem********** -->