<?php

session_start();

$_SESSION['aktif'] = '';
unset($_SESSION['aktif']);
session_unset();
session_destroy();
header("Location: index.php");

?>