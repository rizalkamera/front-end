<?php
include 'koneksi.php';
include 'tanggal_indo.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
	
	$s = mysqli_query($mycon, "SELECT * FROM user p, notasewa n
	                           WHERE p.id = n.user_id AND p.username = '$pengguna' AND n.statusnota = 'selesai' AND p.hapuskah = '0' AND  n.hapuskah = '0' AND n.statuskembali ='0'");

	$row = mysqli_num_rows($s);

	//jika jml baris < 1, artinya pelanggan belum pernah transaksi sama sekali
	if ($row < 1) {
		echo '<script language="javascript">';
	    echo 'document.location.href="index.php"';
	    echo '</script>';
	}
} 

?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pembayaran | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head><!--/head-->

<body>
	<?php include 'header.php' ?>	

<section id="cart_items">
		<div class="container">
			<div class="step">
				<ul class="steps">
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pembayaran</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>2. Konfirmasi Pembayaran</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>3. Upload Bukti Pembayaran</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>4. Selesai</li>
				</ul>
				<br>
			</div>
			<center><p style="font-size: 25px;">Transaksi anda telah selesai diproses.<br> Terima kasih telah meminjam Peralatan di KURNIA KAMERA.</p></center>
			<br></br>
			<div class="row">
				<div class="success">
					<center><h3>Detail Pesanan Anda</h3></center>
					<br></br>
					 <div class="success-float" style="margin-left: 10%;">
				        <?php
				         $q = mysqli_query($mycon, "SELECT k.nama, k.alamat, k.notlp, h.persyaratan FROM user k, notasewa h WHERE  h.user_id = k.id AND k.username = '$pengguna' AND h.statuskembali ='0'"); 	
							
						if(!$q)
						{
							echo mysqli_error($mycon);

						}
						while($w = mysqli_fetch_array($q))
						{
					
						echo '
						<h4>Nama Pemesan : &nbsp;&nbsp;' .$w['nama']. '<br><br>

						<p>Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;' .$w['alamat']. '<br><br>

						<p>Nomor Hp &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;' .$w['notlp']. '<br><br>

						<p>Persyaratan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;' .$w['persyaratan']. '<br></br>';
						
							} 

						 ?>
						 
						
						<br></br>
						<br>
					</div>
				</div>
			</div>
			<div class="review-payment">
				<h2>Detil Transaksi</h2>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td style= text-align:center class="image">Produk</td>
							<td style= text-align:center class="description">Nama Barang</td>
							<td style= text-align:center class="price">Tanggal Ambil</td>
							<td  style= text-align:center class="quantity">Durasi</td>
							<td style= text-align:center  class="quantity">Tanggal Kembali</td>
							<td style= text-align:center class="total">Subtotal</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<?php
					$q = mysqli_query($mycon, "SELECT *, SUM(d.hargasewa*d.jmlsewa) as subtotal 
					FROM  kamera c, hub_notasewa_dan_kamera d, notasewa e, user p
					WHERE c.id = d.kamera_id AND d.nota_id = e.id AND e.user_id = p.id AND p.username = '$pengguna' AND e.statuskembali ='0' group by c.id");
					while ($row = mysqli_fetch_array($q)) {
					    echo '
						<tr>
							<td class="cart_total">
								<p align=center <a href=""> <img src="../bismillah/images/' .$row['gambar']. '" width="100" height="100" /></a></p>
							</td>
							<td class="cart_total">

								<p align=center > <a href=""> ' . $row['namakamera'] . '</a></p>
								<p align=center>'.$row['jmlsewa'].'pcs (@Rp. '.number_format($row['hargasewa'], 0, ',', '.') . ',-)</p>
							</td>
							<td class="cart_total">
								<p align=center>' .TanggalIndoWithTime($row['tgl_ambil']). '</p>
							</td>
							<td class="cart_total">
								<p align=center>' .$row['durasi']. ' jam</p>
							</td>';

							$date_ambil = $row['tgl_ambil'];
							$date_kembali = '';
							$date_kembali = date('Y-m-d H:i:s',strtotime('+'.$row['durasi'].' hour',strtotime($date_ambil)));
							// echo date_format($date, 'Y-m-d');

							echo '
							<td class="cart_total">
								<p align=center>' .TanggalIndoWithTime($date_kembali). '</p>
							</td>
							<td class="cart_total">
								
								<p align=center> Rp. ' . number_format($row['subtotal'], 0, ',', '.') . ',-' . '</p>
							</td>
						</tr>';
					} ?>

						
						<tr class="checkout_akumulasi">
							<td colspan="4"></td>
								<?php
								//$sq = mysqli_query($mycon, "SELECT SUM(s.subtotal) as total from (SELECT b.nama_barang, b.jpeg_file, b.harga, b.stok, h.nota_id, h.barang_id, h.jml_jual, h.harga_jual, SUM(h.jml_jual*h.harga_jual) as subtotal FROM pelanggan p, nota_jual n, hub_nota_barang h, barang b WHERE p.id_pel = n.pelanggan_id AND n.id_notaJual = h.nota_id AND h.barang_id = b.id_barang AND p.username = '" . $pengguna . "' AND n.status_pesanan = 'menunggu pembayaran' AND n.hapuskah = '0' group by b.nama_barang ) AS s");
								$q = mysqli_query($mycon, "SELECT SUM(d.hargasewa*d.jmlsewa) as subtotal FROM kamera c, hub_notasewa_dan_kamera d, notasewa e, user p WHERE c.id = d.kamera_id AND d.nota_id = e.id AND e.user_id = p.id AND p.username = '$pengguna' AND e.statuskembali ='0' group by e.id");
								while ($row1 = mysqli_fetch_array($q)) {
								    //buat input type hidden yang nyimpen total
								    echo '
								<input type="hidden" id="total" value="' . $row1['subtotal'] . '" />
								<td><h4>Total Pembayaran</h4></td>
								<td><h4 align=center >Rp. ' . number_format($row1['subtotal'], 0, ',', '.') . ',-</h4></td>';
								}?>
						</tr>
						
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-sm-4 col-sm-offset-4">
				<div class="btn">
					<a href="index.php"><button>Kembali ke Beranda</button></a>
					</form>
					<br></br>
					<br></br>
				</div>
			</div>
		</div>
		
	</section> <!--/#cart_items-->
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->