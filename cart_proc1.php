<?php

include 'koneksi.php';
session_start();

if(empty($_SESSION['aktif']))
{
	echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];

	// inisialisasi
	$id_nota = 0;
	$item_id = 0;
	$tgl_ambil = '';
	$date_kembali='';
	$qty = 0;
	$harga_sewa = 0;
	$durasi = 0;
	
	//langsung tembak id pel 4
	$id_pel = 0;

	$bar = 0;


	if (isset($_POST['sbmt_form'])) 
	{
		
		$item_id = $_POST['item_ID'];
	    
	    $tgl_ambil = $_POST['ambil'];
		
		$qty = $_POST['jml_item'];

		
	    $harga_sewa_temp = $_POST['harga'];
	    //pisahkan harga sewa dan opsi yang dipilih (substring dari blakang)
	    $hasil_split = explode("-",$harga_sewa_temp);
	    $harga_sewa = $hasil_split[0];
	    $durasi = $hasil_split[1];

	    //convert opsi yg dipilih(1/2/3) menjadi durasi sebenarnya (6,12,24)
	    if ($durasi == 1) {
	    	$durasi = 6;
	    }
	    else if ($durasi == 2) {
	    	$durasi = 12;
	    }
	    else if ($durasi == 3) {
	    	$durasi = 24;
	    }

	  
		
	}
	
		
	

	//sebelum masuk ke tabel nota, ambil id pelanggan terlebih dahulu
	$s0 = mysqli_query($mycon,"SELECT id FROM user WHERE username = '" .$pengguna."' AND hapuskah = '0'");
	while($sq = mysqli_fetch_array($s0))
	{
		$id_pel = $sq['id'];
	}

	//cek tabel nota, apaka pengguna ini punya transaksi lain yg belum diselesaikan (yang statusnya menunggu pembayaran dan menunggu bukti transfer)
	//yang statusnya tidak terhapus (hapuskah = 0)
		
	//STATUS NOTA
	//menunggu barang sewa 			= pelanggan masih memilih barang sewa untuk dimasukkan ke keranjang sewa
    //menunggu data sewa 			= pelanggan belum mengisi form data penyewa 
    //menunggu data pembayaran		= pelanggan belum mengisi data pembayaran
    //menunggu validasi pembayaran 	= pelanggan sudah mengisi data pembayaran dan menunggu validasi dari admin
    //selesai 						= proses booking sudah selesai dan pelanggan dapat membuat transaksi sewa yang baru lagi



	//cek apakah ada transaksi yang belum selesai
	//yang statusnya "menunggu validasi pembayaran", "menunggu data pembayaran", atau "menunggu data sewa"
	$s = mysqli_query($mycon,"SELECT * FROM notasewa WHERE user_id = '" .$id_pel. "' and statusnota in ('menunggu validasi pembayaran','menunggu data pembayaran','menunggu data sewa') AND hapuskah = 0");
	if(!$s)
	{
		echo '1' .mysqli_error($mycon);
	}
	$s1 = mysqli_num_rows($s);
	//jika ada
	if($s1 > 0)
	{
		//error tidak bisa menambahkan barang. Selesaikan transaksi yang sudah ada. 
		echo '<script language="javascript">';
	    echo 'window.alert("Tidak dapat menambahkan barang sewa ke keranjang. Selesaikan transaksi sebelumnya terlebih dahulu. Terima kasih.");';
	    //diarahkan ke halaman pembayaran
	    //echo 'window.alert("nunggu pembayaran, beli brg dari index/prod det.");';
	    echo 'document.location.href="index.php"';
	    echo '</script>';
	}
	//jika tidak ada
	else if($s1 < 1)
	{
		echo '<script language="javascript">';
	    
	    echo '</script>';

		//cek apakah ada transaksi yang statusnya "menunggu barang sewa"
		//yang statusnya tidak terhapus (hapuskah = 0)
		$sa = mysqli_query($mycon,"SELECT * FROM notasewa WHERE user_id = '" .$id_pel. "' and statusnota = 'menunggu barang sewa' AND hapuskah = '0'");
		$s2 = mysqli_num_rows($sa);
		//jika ada nota yang statusnya memilih barang
		if($s2 > 0)
		{	
			

			//ambil id nota yg statusnya memilih barang tadi
			while($sq = mysqli_fetch_array($sa))
			{
				$id_nota = $sq['id'];
			}


			//cek apakah di nota tsb, barang yg dipesan sudah tercantum atau blm
			//yang statusnya tidak terhapus (hapuskah = 0)
			$po = mysqli_query($mycon, "SELECT * FROM hub_notasewa_dan_kamera where nota_id ='" .$id_nota. "' and kamera_id = '" .$item_id. "'");
			$cekBarang = mysqli_num_rows($po);
			//jika ada barang yang sama

			// BARANG SUDAH ADA di kranjang
			if($cekBarang > 0)
			{
				//update jumlah barang tsb
				echo '<script language="javascript">';
			    echo 'window.alert(" barang sudah ada di kranjang silahkan akses halaman keranjang untuk mengatur barang sewa anda. ");';
			    // echo 'window.alert("nunggu selesai belanja, brg sdh ada di nota, dari prod det.");';
			    echo 'document.location.href="index.php"';
			    echo '</script>';
				
			}

			//BARANG TIDAK ADA di kranjang
			//jika barang yg ditambahkan belum ada di keranjang belanja
			else if($cekBarang < 1)
			{

				//insert barang ke nota yg dibuat tadi
				$q1 = mysqli_query($mycon, "INSERT INTO hub_notasewa_dan_kamera (nota_id,kamera_id,rusak,hilang,denda,tgl_ambil,hargasewa,durasi,jmlsewa, keranjang) 
											VALUES ('$id_nota','$item_id',0,0,0,'$tgl_ambil',$harga_sewa,$durasi,$qty,1)");
				
				if(!$q1)
				{
					echo 'q1'. mysqli_error($mycon);
				}

					echo '<script language="javascript">';
				    echo 'window.alert("item telah ditambahkan ke keranjang.");';
				    // echo 'window.alert("nunggu selesai belanja, brg blm prnah dibeli, dari prod det.");';
				    echo 'document.location.href="index.php"';
				    echo '</script>';
			}
		}
		//jika tidak ada nota yang statusnya memilih barang (transaksi baru)
		else if($s2 < 1)
		{
			//ambil tanggal system
			date_default_timezone_set('Asia/Bangkok');
            $tanggalan  = date('Y-m-d H:i:s');

			
			//insert nota baru
			$qq = mysqli_query($mycon, "INSERT INTO notasewa (tanggalpesan, grandtotal,statusnota,dendaterlambat,dendakerusakan,dp,pelunasan,user_id,hapuskah) VALUES ('$tanggalan',0,'menunggu barang sewa',0,0,0,'belum lunas','$id_pel',0)");
			if(!$qq)
			{
				echo '01' .mysqli_error($mycon);
			}
			else
			{
				
			}

			//insert barang dengan id nota yg baru

			//ambil id nota yang baru tadi
			$sw = mysqli_query($mycon,"SELECT id FROM notasewa WHERE hapuskah = '0' order by id desc limit 1");
			if(!$sw)
			{
				echo '02' .mysqli_error($mycon);
			}
			while($sq = mysqli_fetch_array($sw))
			{
				$nota_baru = $sq['id'];
			}
			


			//insert barang ke nota yg dibuat tadi
			$q = mysqli_query($mycon, "INSERT INTO hub_notasewa_dan_kamera (nota_id,kamera_id,rusak,hilang,denda,tgl_ambil,hargasewa,durasi,jmlsewa,keranjang) 
				VALUES ('$nota_baru','$item_id',0,0,0,'$tgl_ambil',$harga_sewa,$durasi,$qty,1)");
			if(!$q)
			{
				echo '03' .mysqli_error($mycon);
			}
			
			echo '<script language="javascript">';
		    echo 'window.alert("item telah ditambahkan ke keranjang.");';
		    // echo 'window.alert("transaksi baru, beli brg dari prod det.");';
		    echo 'document.location.href="index.php"';
		    echo '</script>';
			
		}
	}
 }


?>