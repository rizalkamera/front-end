<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
	$s = mysqli_query($mycon, "SELECT * FROM user p, lelang l WHERE p.id = l.pelanggan_id AND p.username = '$pengguna' AND l.statuspembayaran = 'selesai' AND p.hapuskah = '0' AND l.hapuskah = '0' AND l.buktivalid ='1' AND l.statusmenang = '0'");
	$row = mysqli_num_rows($s);

	//jika jml baris < 1, artinya pelanggan belum pernah transaksi sama sekali
	if ($row < 1) {
		echo '<script language="javascript">';
	    echo 'document.location.href="index.php"';
	    echo '</script>';
	}
} 

?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pembayaran | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h1><font color="#9C9B9B">Kurnia</font><font color="#FE980F">Kamera</font></h1></a>
						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
					            		<li><a href="checkout.php">
					            			<div class="mark">Pembelian Barang</div>
					            		</a></li>
					            		<li><a href="checkout_lelang.php">
					            			<div class="mark">Transaksi Lelang</div>
					            		</a></li> 
					            	</ul>
					          	</li>
								<li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
								<?php 
								if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	</header><!--/header-->

<section id="cart_items">
		<div class="container">
			<div class="step">
				<ul class="steps">
					
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pembayaran</li>
				
				  <li class="select"><i class="fa fa-check-square-o"></i>2. Upload Bukti Pembayaran</li>
				  <li class="select"><i class="fa fa-check-square-o"></i>3. Selesai</li>
				</ul>
				<br>
			</div>
			<center><p style="font-size: 20px;">Transaksi anda telah selesai diproses. Terima kasih telah berpartisipasi dalam Event Lelang kami.</p></center>
			<center><p><p style="font-size: 18px;">Barang Bisa di ambil di kurnia kamera paling lambat 3 X 24 jam setelah pembayaran selesai.</p></center>
			<br></br>
			<div class="row">
				<div class="succes">
					<center><h3>Produk Lelang yang Anda Menangkan</h3></center>
					<br></br>
					
					<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td style= text-align:center class="image">Produk</td>
							<td style= text-align:center class="description">Nama Barang</td>
							<td style= text-align:center class="description">Tanggal Pemenangan Lelang</td>
							<td style= text-align:center class="total">Biaya Bayar</td>
							<td></td>
						</tr>
					</thead>
							<tbody>
							<?php

							//ambil transaksi lelang yang paling baru (dilihat dari waktu selesai paling baru) yang statusnya selesai
							$sql = mysqli_query($mycon,"SELECT * FROM lelang l, user p WHERE l.pelanggan_id = p.id AND l.statuspembayaran = 'selesai' AND p.username = '" .$pengguna. "' AND l.buktivalid ='1' AND l.statusmenang = '0'");
							while($row = mysqli_fetch_array($sql))
							{
								echo '
								
									<tr>
							<td class="cart_total">
								<p align=center <a href=""> <img src="../bismillah/images/' .$row['gambarbarang']. '" width="100" height="100" /></a></p>
							</td>
							<td class="cart_total">

								<h4 align=center > ' . $row['namabarang'] . '</a></h4>
								
							</td>
							<td class="cart_total">

								<h4 align=center > ' . $row['tanggalakhir'] . '</a></h4>
								
							</td>
							<td class="cart_total">
								<h4 align=center>Rp. ' .number_format($row['hargatertinggi'], 0, ',', '.'). ',-'. '</h4>
							</td>
							<td class="cart_total">
						
						</tr>';
								
							} ?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="col-sm-4 col-sm-offset-4">
				<div class="btn">
					<a href="index.php"><button>Kembali ke Beranda</button></a>
					</form>
					<br></br>
					<br></br>
				</div>
			</div>
		</div>
		
	</section> <!--/#cart_items-->

	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->