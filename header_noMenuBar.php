<header id="header"><!--header-->		
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="index.php"><h1><font color="#00000">KURNIA </font><font color="#F88017">KAMERA</font></h1></a>
                    </div>	
                </div>
                <div class="col-sm-5 col-sm-offset-3">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="checkout.php">
                                        <div class="mark">Pemesanan Kamera</div>
                                    </a></li>
                                    <li><a href="checkout_lelang.php">
                                        <div class="mark">Transaksi Lelang</div>
                                    </a></li> 
                                </ul>
                            </li>
                            <li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                            <?php 
                            if(isset($_SESSION['aktif']))
                            {
                    echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="logout.php">
                                        <div class="mark">Logout</div>
                                    </a></li>  
                                </ul>
                            </li>';
                            }
                            else if(empty($_SESSION['aktif']))
                            {
                    echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    
</header><!--/header-->