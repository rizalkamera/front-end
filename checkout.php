<?php
include 'koneksi.php';
include 'tanggal_indo.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php
$item_id = 0;
	$qty = 0;
	
	//langsung tembak id pel 4
	$id_pel = 0;

	$id_nota = 0;
	 $jml = 0;
	 $bar = 0;
include 'koneksi.php';
//echo $_SESSION['aktif'];
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
} else if (isset($_SESSION['aktif'])) {
    $pengguna = $_SESSION['aktif'];
}

$s = mysqli_query($mycon, "SELECT * FROM user p, notasewa n
						   WHERE p.id = n.user_id AND p.username = '$pengguna' AND n.statusnota != 'selesai' AND p.hapuskah = '0' AND  n.hapuskah = '0'");
	
//tanpa login
//$s = mysqli_query($mycon, "SELECT * FROM notasewa n WHERE statusnota != 'selesai'");
while ($q = mysqli_fetch_array($s)) {
    $stat  = $q['statusnota'];
    $valid = $q['statuspesanan'];
    $bukti = $q['buktitransfer'];
}

$cek_data = mysqli_num_rows($s);
//jika tidak ada, ada 3 kemungkinan
//1. pelanggan belum pernah melakukan transaksi sama sekali
//2. semua transaksi yang dilakukan sudah selesai
//3. ada transaksi yang belum selesai
if ($cek_data < 1) {

	$s = mysqli_query($mycon, "SELECT * FROM user p, notasewa n
	                          WHERE p.id = n.user_id AND p.username = '$pengguna' AND n.statusnota = 'selesai' AND p.hapuskah = '0' AND  n.hapuskah = '0'");
	$row = mysqli_num_rows($s);

	// jika tidak ada, artinya pelanggan belum pernah transaksi sama sekali
	if ($row < 1) {
		echo '<script language="javascript">';
	    echo 'document.location.href="checkout_empty.php"';
	    echo '</script>';
	}
	//jika ada, maka tampilkan transaksi terakhir yang selesai
	else{
		echo '<script language="javascript">';
	    echo 'document.location.href="success.php"';
	    echo '</script>';
	}
}

else 
{
	// echo '<script language="javascript">';
    // echo 'window.alert(">1");';
	// echo '</script>';
	
    //cek status transaksinya
    if ($stat == 'menunggu barang sewa') {
        echo '<script language="javascript">';
        echo 'document.location.href="checkout_notproceed.php"';
        echo '</script>';
	} 
	else if ($stat == 'menunggu data pembayaran') {
        echo '<script language="javascript">';
        echo 'document.location.href="checkout_done.php"';
        echo '</script>';
    } 
    else if ($stat == 'menunggu validasi pembayaran') {
        //cek apakah yang menunggu bukti transfer itu status buktinya kosong atau terisi 0

        //kalo kosong, artinya pelanggan belum mengupload bukti transfer, atau sudah mengupload tapi belum divalidasi
    	//jika status bukti kosong dan bukti transfer belum terisi, maka pelanggan belum melakukan upload
    	//jika status bukti kosong tapi bukti transfer sudah terisi, maka pelanggan sudah melakukan upload namun belum divalidasi oleh admin

        //kalo terisi 0, artinya bukti transfer yang diupload tidak valid, dan pelanggan harus mengu
        if ($valid == "") {
        	//jika status bukti kosong dan bukti transfer kosong, maka alihkan ke checkout_done.php
        	if ($bukti == '') {
        		echo '<script language="javascript">';
	            echo 'document.location.href="checkout_done.php"';
	            echo '</script>';
        	}

        	//jika status bukti kosong dan bukti transfer terisi, maka alihkan ke on_process.php
        	else{
        		echo '<script language="javascript">';
	            echo 'document.location.href="on_processpesan.php"';
	            echo '</script>';
        	}
        }
        elseif ($valid == "0") {
        	echo '<script language="javascript">';
            echo 'document.location.href="reupload.php"';
            echo '</script>';
		}
		else
		{
			echo '<script language="javascript">';
			// echo 'window.alert("gagal");';
			 echo 'document.location.href="index.php"';
			echo '</script>';
		}
    } 
 }


?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- untuk radiobutton event -->
	<script src="js/jquery.min.js"></script>
	<!-- untuk radiobutton event -->

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Checkout | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">

</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h1><font color="#9C9B9B">KURNIA </font><font color="#FE980F">KAMERA</font></h1></a>
						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
					            		<li><a href="checkout.php">
					            			<div class="mark">Pemesanan Barang</div>
					            		</a></li>
					            		<li><a href="checkout_lelang.php">
					            			<div class="mark">Transaksi Lelang</div>
					            		</a></li> 
					            	</ul>
					          	</li>
								<li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
								<?php 
								if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="step">
						<ul class="steps">
				  <li class="select"><i class="fa fa-check-square-o"></i>1. Pimilihan Durasi Sewa</li>
				  <li class="select"><i class="fa fa-check-square-o"></i> 2.  Mengisi Form Data Sewa</li>
				  <li >3. Pilih Pembayaran</li>
				  <li >4. Upload Bukti Pembayaran</li>
				  <li >5. Selesai</li>
				</ul>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->

	<section id="cart_items">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="cust">
						<p>PILIH PERSAYARATAN YANG AKAN DI GUNAKAN</p>
						<form method="POST" action="">
							<select style="width: 100%" id="cboalamat" name="persyaratan" required>
								<?php
								$sq = mysqli_query($mycon, "SELECT persyaratan from notasewa");
								while ($row = mysqli_fetch_array($sq)) {
								}
								?>
                                   <option value="KTP dan Kartu Keluarga" <?php if($row['persyaratan'] == 'KTP dan Kartu Keluarga'){ echo 'selected'; } ?>>KTP dan Kartu Keluarga</option>
                                   <option value="SIM dan STNK" <?php if($row['persyaratan'] == 'SIM dan STNK'){ echo 'selected'; } ?>>SIM dan STNK</option>
 
                                </select>
							
					</div>
				</div>
			</div>
			<div class="review-payment">
				<h2>Tinjau Ulang Barang dan Biaya Total Pesanan</h2>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td style= text-align:center class="image">Produk</td>
							<td style= text-align:center class="description">Nama Barang</td>
							<td style= text-align:center class="price">Tanggal Ambil</td>
							<td  style= text-align:center class="quantity">Durasi</td>
							<td style= text-align:center name="tanggalkembali" class="quantity">Tanggal Kembali</td>
							<td style= text-align:center class="total">Subtotal</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<?php
					
					$q = mysqli_query($mycon, "SELECT kat.id as kat_id, e.id as nota_id, c.id as kamera_id, c.namakamera, c.gambar, d.hargasewa as harga1, d.jmlsewa, d.tgl_ambil, d.durasi, SUM(d.hargasewa*d.jmlsewa) as subtotal 
						FROM kategori kat, kamera c, hub_notasewa_dan_kamera d, notasewa e, user p
						WHERE kat.id = c.kategori_id and c.id = d.kamera_id AND d.nota_id = e.id AND e.user_id = p.id AND p.username = '$pengguna' AND d.keranjang = '1' group by c.id");
				
				
					while ($row = mysqli_fetch_array($q)) {
					    echo '
						<tr>
							<td class="cart_total">
								<p align=center <a href=""> <img src="../bismillah/images/' .$row['gambar']. '" width="100" height="100" /></a></p>
							</td>
							<td class="cart_total">

								<p align=center > <a href=""> ' . $row['namakamera'] . '</a></p>
								<p align=center>'.$row['jmlsewa'].'pcs (@Rp. '.number_format($row['harga1'], 0, ',', '.') . ',-)</p>
							</td>
							<td class="cart_total">
								<p>' .TanggalIndoWithTime($row['tgl_ambil']). '</p>
							</td>
							<td class="cart_total">
								<p align=center>' .$row['durasi']. ' jam</p>
							</td>';

							$date_ambil = $row['tgl_ambil'];
							$date_kembali = '';
							$date_kembali = date('Y-m-d H:i:s',strtotime('+'.$row['durasi'].' hour',strtotime($date_ambil)));
						
							echo '
							<td class="cart_total">
								<p align=center>' .TanggalIndoWithTime($date_kembali). '</p>
							</td>
							<td class="cart_total">
								
								<p align=center> Rp. ' . number_format($row['subtotal'], 0, ',', '.') . ',-' . '</p>
							</td>
						</tr>';
					}?>
						<tr class="checkout_akumulasi">
							<td colspan="4"></td>
								<?php
								$q = mysqli_query($mycon, "SELECT SUM(d.hargasewa*d.jmlsewa) as subtotal FROM kamera c, hub_notasewa_dan_kamera d, notasewa e, user p WHERE c.id = d.kamera_id AND d.nota_id = e.id AND e.user_id = p.id AND p.username = '$pengguna' AND d.keranjang ='1' group by e.id");
								while ($row1 = mysqli_fetch_array($q)) {
								    //buat input type hidden yang nyimpen total
								    echo '
								<input type="hidden" id="total" name="total" value="' . $row1['subtotal'] . '" />
								<td><h4>Total Biaya yang Harus Dibayar</h4></td>
								<td><h4>Rp. ' . number_format($row1['subtotal'], 0, ',', '.') . ',-</h4></td>';
								}?>
					</tr>
						<tr class="checkout_akumulasi">
						</tr>
						<tr class="checkout_akumulasi">
							<td colspan="3"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-sm-4 col-sm-offset-5">
				<div class="btn">
					<button type="submit" name="simpn" >Selesai Pesan</button>
					</form>
					<br></br>
					<br></br>
				</div>
			</div>
		</div>

	</section> <!--/#cart_items-->
<script type="text/javascript">
		var mycbo = document.getElementById('cmbdurasi');
		var hargasewa2 = document.getElementById('hargasewa1');
		var temp = '';

		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}

		mycbo.onchange = function(){
			// alert(this.value);
			// alert(formatRupiah(this.value, 'Rp. '));
			document.getElementById("hargasewa1").innerHTML = formatRupiah(this.value, 'Rp. ');



	//*********set alamat************
		var alamat = document.getElementById('alamat');
		var mycbo = document.getElementById('cboalamat');

		mycbo.onchange = function(){
			//jika user memilih comboboc alamat lain (alamat lain valuenya aa),
			//maka textarea di disabled false (diaktifkan)
		     if (mycbo.value === "aa") {
		     	alamat.value = "";
		        document.getElementById('alamat').disabled='';
		    } else {
		    	alamat.value = "";
		     	alamat.value = alamat.value + this.value;
		    	document.getElementById('alamat').disabled='true';
		    }
		}
	//**********end of set alamat**********8
		}
	</script>

	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->

    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>

    <script src="js/jquery.min.js"></script>
</body>
</html>


<?php

// lalu ada link "lanjutkan transaksi" yang menuju ke checkout_done.php
include 'koneksi.php';

if(isset($_POST['simpn']))
{

	$grandtot = $_POST['total'];
	//awalnya simpan value alamat dari combobox
    $persyaratan = $_POST['persyaratan'];
	
	$sw = mysqli_query($mycon,"SELECT n.id FROM notasewa n, user p, hub_notasewa_dan_kamera q WHERE n.user_id = p.id AND p.username = '".$pengguna."' AND  q.nota_id = n.id AND n.hapuskah = '0'  AND p.hapuskah = '0' order by id desc limit 1");
	while($sq = mysqli_fetch_array($sw))
	{
		$bar = $sq['id'];
	}
					
    $e = mysqli_query($mycon, "UPDATE notasewa set  persyaratan = '$persyaratan', statusnota = 'menunggu data pembayaran', grandtotal = '$grandtot' where id = '$bar'");
    
    if(!$e)
    {
        echo 'error ' .mysqli_error($mycon);
    }
   $q = mysqli_query($mycon, "UPDATE hub_notasewa_dan_kamera  SET tgl_kembali = '$date_kembali', keranjang = '0' ");  
    
     if(!$q)
     {
    	echo 'error ' .mysqli_error($mycon);
 	 }
 	 else
 	 {
    	 echo '<script language="javascript"> 
      	alert("Data Berhasil diinput.")
      	document.location.href="checkout_done.php"
      	</script>';
    }
}

?>