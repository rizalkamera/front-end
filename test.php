
<!-- ***********Passing value lewat button************** -->
<?php

if(isset($_GET['submit']))
{
	$number = $_GET['key'];
	echo $number;
}  ?>
<form method="GET" action="http://somewhere.com" >
    
    <!-- ****kondisi 1 (hanya set id pada button, ada 2 parameter yang dikirim)*** -->
    <!-- <input type="text" placeholder="XXX" name="key"> -->
    <!-- <input type="hidden" name="key1" value="aa"> -->
    <!-- <button type="submit" id="submit">Submit</button> <br> -->
    <!-- inputan = asas -->
    <!-- hasil => https://www.somewhere.com/?key=asas&key1=aa -->
    

    <!-- *****kondisi 2 (hanya set id pada button, ada 1 parameter yang dikirim)**** -->
    <input type="text" placeholder="XXX" name="key">
    <button type="submit" id="submit">Submit</button> <br>
    <!-- inputan = qwerty -->
    <!-- hasil => http://someurl.com/?key=qwerty -->


    <!-- ****kondisi 3 (set name pada button)****** -->
    <!-- <input type="text" placeholder="XXX" name="key"> -->
    <!-- <button type="submit" name="submit">Submit</button> -->
    <!-- inputan = qwerty -->
    <!-- hasil => http://someurl.com/?key=qwerty&submit= -->

    <!-- *****kondisi 4 (set name di input type button)***** -->
    <!-- <input type="text" placeholder="XXX" name="key"> -->
    <!-- <input type="submit" name="submit" /> -->
    <!-- inputan = qwerty -->
    <!-- hasil => http://someurl.com/?key=qwerty&submit=Submit -->
</form> 
<!-- ***********end of passing value lewat button********* -->





<!-- *********ganti value textbox berdasarkan radiobutton success************ -->

<!-- <input type="radio" class="name" name="paket3" value="19000" /> Regular<br>
<input type="radio" class="name" name="paket2" value="21000" /> YES<br>
<input type="text" name="ongk1" id="ongk"  /> -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
 <script>
    $('.paket1').change(function(e){
        var selectedValue = $(this).val();
        $('#ongk').val(selectedValue)
    });
 </script> -->
<!-- ********end of ganti value textbox berdasarkan radiobutton success******** -->




<!-- ********sama kaya atasnya ini, hasilnya juga sama******* -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<input type="radio" class="name" name="paket3" value="19000" /> Regular<br>
<input type="radio" class="name" name="paket2" value="21000" /> YES<br>
<input type="text" name="ongk1" id="ongk"  />

<script>
$('.name').on('change', function() {
  $('#ongk').val( this.value );
});
</script> -->

<!-- *******end of sama kaya atasnya ini******** -->




<!-- ********ganti html berdasarkan radiobutton yang dipilih (untuk lebih dari 1 kategori radiobutton)**********-->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script>
$(document).ready(function() {
  $("input[type=radio]").on("change", function() {
    var results = "";

    $("input:checked").each(function(key, val) {
      results += $(val).val() + " "; //untuk menampilkan hasil dari 3 kategori radiobutton
      // results = $(val).val();        //untuk menampilkan hasil dari hanya 1 kategori radiobutton
    });
    
    $(".results").html(results);
  });
});
</script> 
<input type="radio"  name="ac" value="yes"> YES
<input type="radio"  name="ac" value="no"> NO
<br>
<input type="radio"  name="tier" value="normal"> normal
<input type="radio"  name="tier" value="deluxe"> deluxe
<br>
<input type="radio"  name="cap" value="big"> big
<input type="radio"  name="cap" value="small"> small<br>

<div class="results"></div> -->


<!-- *********end of ganti html berdasarkan radiobutton yang dipilih (untuk lebih dari 1 kategori radiobutton)********** -->




<!-- ********* sama kaya atasnya, cuman ini versi simple nya :) ************* -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script>
$(document).on("change","input[type=radio]",function(){

    var ac=$('[name="ac"]:checked').val();
    
    $(".results").html(ac);
});
</script>
<form>
<input type="radio"  name="ac" value="yes"> YES
<input type="radio"  name="ac" value="no"> NO
<br>
<input type="radio"  name="tier" value="normal"> normal
<input type="radio"  name="tier" value="deluxe"> deluxe
<br>
<input type="radio"  name="cap" value="big"> big
<input type="radio"  name="cap" value="small"> small
</form>
<span class="results"></span>

<!-- ********* end of sama kaya atasnya, cuman ini versi simple nya :) ************* -->





<!-- *******ganti text html berdasakan input textbox******** -->

 <!-- <label>Enter your name: <input class="name" type="text" value="World"/></label> -->
 <!-- Enter your name: <input class="name" type="text" value="World"/> -->
  
  <!-- Plain Javascript Example -->
  <!-- <h1>Plain Javascript Example</h1>Hello <span class="jsValue">World</span> -->
  <!-- <h1>Plain Javascript Example</h1>Hello <div class="jsValue">World</div> -->
  
  <!-- jQuery Example -->
  <!-- <h1>jQuery Example</h1>Hello <span class="jqValue">World</span> -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script>
// Plain Javascript Example
var $jsName = document.querySelector('.name');
var $jsValue = document.querySelector('.jsValue');

$jsName.addEventListener('input', function(event){
  $jsValue.innerHTML = $jsName.value;
}, false);


 // JQuery example
var $jqName = $('.name');
var $jqValue = $('.jqValue');

$jqName.on('input', function(event){
  $jqValue.html($jqName.val());
});
</script>
 -->
<!-- *********end of ganti text html berdasarkan input textbox********** -->




<!-- *********contoh ganti value textbox berdasarkan radiobutton yang dipilih********* -->

<!-- <input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="5" />
<input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="10" />
<input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="15" />
<input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="20" />

<input type="text" name="amount" id="amount" />

<script>
$('.radiogroup').change(function(e){
    var selectedValue = $(this).val();
    $('#amount').val(selectedValue)
});
</script>
 -->

<!-- *******end of contoh ganti value textbox berdasarkan radiobutton yang dipilih*******-->




<!-- *********validasi password********** -->
<!-- <form method="post" action="test.php">
<input type="password" name="pw" required />
<input type="password" name="repass" required />
<input type="submit" name="submit" />
</form> -->


<?php
// if(isset($_POST['submit']))
// {
//   if($_POST["pw"] == $_POST["repass"]) {
//       $password = $_POST["pw"];
//       $cpassword = $_POST["repass"];
//       if (strlen($_POST["pw"]) < '8') {
//           // $passwordErr = "Your Password Must Contain At Least 8 Characters!";
//           echo '<script language="javascript">'. 
//       'window.alert("Maaf, Password setidaknya memiliki panjang 8 karakter.")'.
//       '</script>';
//       }
//       elseif(!preg_match("#[0-9]+#",$password)) {
//           // $passwordErr = "Your Password Must Contain At Least 1 Number!";
//           echo '<script language="javascript">'. 
//       'window.alert("Maaf, Password setidaknya mengandung 1 digit angka.")'.
//       '</script>';
//       }
//       elseif(!preg_match("#[A-Z]+#",$password)) {
//           // $passwordErr = "Your Password Must Contain At Least 1 Capital Letter!";
//           echo '<script language="javascript">'. 
//       'window.alert("Maaf, Password setidaknya mengandung 1 huruf besar.")'.
//       '</script>';
//       }
//       elseif(!preg_match("#[a-z]+#",$password)) {
//           // $passwordErr = "Your Password Must Contain At Least 1 Lowercase Letter!";
//           echo '<script language="javascript">'. 
//       'window.alert("Maaf, Password setidaknya mengandung 1 huruf kecil.")'.
//       '</script>';
//       }
//   }
// }

  ?>

  <!-- ***********End of Validasi password************** -->


<!-- ************ambil datetime sistem************ -->
<!-- <?php 
//   date_default_timezone_set('Asia/Bangkok');

//   //format 24jam
//   $tgl = date('Y-m-d H:i:s');

//   //format 12jam dengan am pm
//   $tgl1 = date('Y-m-d h:i:sa');


// echo $tgl;
// echo "<br>";
// echo $tgl1;
  ?>

<!-- **********End of ambil datetime sistem************ -->


<!-- ******perbedaan name dan tmp_name********** -->

<form method="post" action="test.php" enctype="multipart/form-data">
<input type="file" name="files" accept="image/jpeg" />
<input type="submit" name="submit" />
</form>

<?php

if(isset($_POST['submit']))
{
    //fungsi split dengan pemisahnya adalah "." dan hasil split dimasukkan ke array $a
    //index 0 isinya nama file
    //index 1 isinya format file
    // $a = explode('.',$_FILES['files']['name']);
    // echo $a[0];
    // echo "<br>";
    // echo $a[1];
    // echo "<br>";
    // echo basename($_FILES['files']['name']);
    // echo "<br>";
    // echo $_FILES['files']['name'];
    // echo "<br>";
    // echo $_FILES['files']['tmp_name'];

    $arr1 = md5($_FILES['files']['name']);
    echo $arr1;
    echo "<br>";
    $split = substr($arr1,0,10);
    echo $split;
    echo "<br>";
    $name = $split . ".jpg";
    echo $name;
    echo "<br>";
}

// *******hasilnya (nama file = photo.jpg)*******

//$a[0] => photo
//$a[1] => jpg
//basename => photo.jpg
// name => photo.jpg
// tmp_name => C:\xampp\tmp\phpDD79.tmp

?>

<!-- ************End of Perbedaan name dan tmp_name************* -->






<!-- ********* ONE  COUNT DOWN TIMER************* -->
<!-- <input type="hidden" id="tglMulai" value="2017-05-11 15:30:00"/> 
<h1><span id="countdown1"></span></h1>
<hr>-->

<!-- <script>
// Set the date we're counting down to
var countDownDate = new Date("2017-05-11 17:51:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "EXPIRED";
    }
}, 1000);
</script> -->

<!-- ***********END OF ONE COUNT DOWN TIME************ -->





<!-- ********MULTIPLE COUNTDOWN TIMER*********** -->

<!-- <span class='countdown' value='2017/05/13 11:57:00'></span><a class="link" href="http://www.facebook.com">Facebook</a><br />
<span class='countdown' value='2017/06/06 23:59:59'></span><br />
<span class='countdown' value='2017/08/07 01:15:15'></span><br />
<script src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.1.0/dist/jquery.countdown.min.js"></script>


<script>
$(function(){
  $('.countdown').each(function(){
    $(this).countdown($(this).attr('value'), function(event) {
      if (event.strftime('%D') == 0) 
      {
        if (event.strftime('%H') == 0) 
        {
          if (event.strftime('%M') == 0) 
          {
            if (event.strftime('%S') == 0) 
            {
              $(this).text('EXPIRED');

              disable link saat waktu habis (expired)
              $('.link').on('click', function() {
                return false;
              });
            }
            else
            {
              $(this).text(event.strftime('%S sec'));
            }
          }
          else
          {
            $(this).text(event.strftime('%M minutes %S sec'));
          }
        }
        else
        {
          $(this).text(event.strftime('%H hours %M minutes %S sec'));
        }
      }
      else
      {
        $(this).text(event.strftime('%D days %H hours %M minutes %S sec'));
      }
    });
  });
});
</script> -->

<!-- *************** END OF MULTIPLE COUNT DOWN TIMER ************** -->




<!-- *********AJAX********* -->

<h1> ajax</h1>

<script src="jquery.js"></script>
<script>
  $(document).ready(function() {
    $('#nama').keyup(function() {
        var nama = $('#nama').val();
        $.ajax ({
          type: "POST",
          url: "t1.php",
          data: 'nama=' +nama,
          success: function(html) {
            $('#tampil').html(html);
          }
        });
      });
  });
</script>

Masukkan nama
<input type="text" name="nama" id="nama" />
<div id="tampil"></div>




<script src="js/jquery.countdown.min.js"></script>

  <!-- ******** SCRIPT COUNT DOWN TIMER********* -->
  <script>
  
  $(function(){
    $('.countdown').each(function(){
      $(this).countdown($(this).attr('value'), function(event) {
        if (event.strftime('%D') == 0) 
        {
          if (event.strftime('%H') == 0) 
          {
            if (event.strftime('%M') == 0) 
            {
              if (event.strftime('%S') == 0) 
              {
                var id = document.getElementById('idlel').value;
                $.ajax({
                  type: "POST",
                  url: "update_lelang.php",
                  data: 'id=' +id,
                  success: function (html){
                    $(this).val('EXPIRED');
                  }
                });
              }
              else
              {
                // var id = document.getElementById('idlel').value;
                // $(this).text(id);
                $(this).text(event.strftime('%S sec'));
              }
            }
            else
            {
              $(this).text(event.strftime('%M minutes %S sec'));
            }
          }
          else
          {
            $(this).text(event.strftime('%H hours %M minutes %S sec'));
          }
        }
        else
        {
          $(this).text(event.strftime('%D days %H hours %M minutes %S sec'));
        }
      });
    });
  });
  </script>
<input type="hidden" id="idlel" value="2" /> 
<span class="countdown" value="2017-05-28 17:59:00"></span>
<div id="cek"></div>