    <?php 
include 'koneksi.php';
include 'tanggal_indo.php';
session_start(); ?>

<!DOCTYPE html>
<html lang="en">



<?php

if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
    $pengguna = $_SESSION['aktif'];
    //cek apakah pelanggan punya transaksi yang statusnya menunggu bukti transfer
   $s = mysqli_query($mycon, "SELECT * FROM user p, notasewa n
                               WHERE p.id = n.user_id AND p.username = '$pengguna' AND n.statusnota = 'selesai' AND p.hapuskah = '0' AND  n.hapuskah = '0' AND n.statuskembali = '1'");
   $row = mysqli_num_rows($s);

    //jika tidak ada, maka akan dialihkan ke beranda
    if ($row < 1) {
     echo '<script language="javascript">';
        echo 'document.location.href="index.php"';
        echo '</script>';
    }
}
    ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
   <?php include 'header.php' ?>

    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="index.php">Beranda</a></li>
                  <li class="active">Histori</li>
                </ol>
            </div>
              <center><h3>History Pemesanan Kamera Yang Anda Pernah Sewa</h3></center>
            
            <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>

                    <tr class="cart_menu">
                            <td style= text-align:center class="image">Produk</td>
                            <td style= text-align:center class="description">Nama Barang</td>
                            <td style= text-align:center class="price">Tanggal Ambil</td>
                            <td  style= text-align:center class="quantity">Durasi</td>
                            <td style= text-align:center  class="quantity">Tanggal Kembali</td>
                            <td style= text-align:center class="total">Subtotal</td>
                            <td style= text-align:center class="total">Edit|Hapus</td>
                            <td></td>
                        </tr>
                </thead>
                <tbody>

        
                <?php
               
               $s = mysqli_query($mycon, "SELECT kat.id as kat_id, e.id as nota_id, c.id as kamera_id, c.namakamera, c.gambar, d.hargasewa as harga1, d.jmlsewa, d.tgl_ambil, d.durasi, SUM(d.hargasewa*d.jmlsewa) as subtotal 
                    FROM kategori kat, kamera c, hub_notasewa_dan_kamera d, notasewa e, user p
                    WHERE kat.id = c.kategori_id and c.id = d.kamera_id AND d.nota_id = e.id AND e.user_id = p.id AND p.username = '$pengguna' AND d.keranjang = '0' AND e.statuskembali = '1' group by c.id");
                if(!$s)
                {
                    echo 'error s' .mysqli_error($mycon);
                }
                 while($row = mysqli_fetch_assoc($s))
                {
                    echo '
                   <td class="cart_total">
                                <p align=center <a href=""> <img src="../bismillah/images/' .$row['gambar']. '" width="100" height="100" /></a></p>
                            </td>
                            <td class="cart_total">

                                <p align=center > <a href=""> ' . $row['namakamera'] . '</a></p>
                                <p align=center>'.$row['jmlsewa'].'pcs (@Rp. '.number_format($row['harga1'], 0, ',', '.') . ',-)</p>
                            </td>
                            <td class="cart_total">
                                <p align=center>' .TanggalIndoWithTime($row['tgl_ambil']). '</p>
                            </td>
                            <td class="cart_total">
                                <p align=center>' .$row['durasi']. ' jam</p>
                            </td>';

                            $date_ambil = $row['tgl_ambil'];
                            $date_kembali = '';
                            $date_kembali = date('Y-m-d H:i:s',strtotime('+'.$row['durasi'].' hour',strtotime($date_ambil)));
                            // echo date_format($date, 'Y-m-d');

                            echo '
                            <td class="cart_total">
                                <p align=center>' .TanggalIndoWithTime($date_kembali). '</p>
                            </td>
                            <td class="cart_total">
                                
                                <p align=center> Rp. ' . number_format($row['subtotal'], 0, ',', '.') . ',-' . '</p>
                            </td>
                    

                        <td class="cart_price">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       
                        
                         
                         <i  class="fa fa-trash-o fa-lg"></i> Hapus</a>
                
                        </td>
                        
                    </tr>';
                    
                } 

                 //mysqli_close($mycon); // menutup koneksi ke database

                    ?>
                    
                
                </tbody>
            </table>
            </div>
        
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
               
                <div class="col-sm-6">
                    
                        
                </div>
            </div>
        </div>
    </section><!--/#do_action-->


    <footer id="footer"><!--Footer-->
        <div class="footer-widget">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="single-widget">
                            <h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera Probolinggo</h1><br>
                            <h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
                            <br></br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer><!--/Footer-->
    


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script src="js/jquery.scrollUp.min.js"></script> -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>



