    <?php 
include 'koneksi.php';
session_start(); ?>

<!DOCTYPE html>
<html lang="en">



<?php

if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
    $pengguna = $_SESSION['aktif'];
    //cek apakah pelanggan punya transaksi yang statusnya menunggu bukti transfer
    $f = mysqli_query($mycon, "SELECT * FROM postinganjualkamera n, user p WHERE n.iduser = p.id AND p.username = '" .$pengguna. "'");

    $res = mysqli_num_rows($f);
    //jika tidak ada, maka akan dialihkan ke beranda
    if ($res < 1) {
     echo '<script language="javascript">';
        echo 'document.location.href="jual.php"';
        echo '</script>';
    }
}
    ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    <header id="header"><!--header-->       
        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="logo pull-left">
                            <a href="index.php"><h1><font color="#0000">KURNIA </font><font color="#FE980F">KAMERA</font></h1></a>
                        </div>  
                    </div>
                   <div class="col-sm-5 col-sm-offset-3">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav">
                                <li class="dropdown"><a href="iklanku.php"><i class='far fa-list-alt' style="color:orange "></i></i>Iklan Saya</i></a>
                                    
                                </li>
                                <li class="dropdown"><a href="postpostingan.php"></i><i class='fa fa-camera' style="color:orange "></i> Jual </a></li>
                                <?php 
                                if(isset($_SESSION['aktif']))
                                {
                        echo '  <li class="dropdown"><a href=""><i class="fa fa-user "style="color:orange"></i> ' .$pengguna. '</a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="logout.php">
                                            <div class="mark">Logout</div>
                                        </a></li>  
                                    </ul>
                                </li>';
                                }
                                else if(empty($_SESSION['aktif']))
                                {
                        echo '  <li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
    
        <div class="header-bottom"><!--header-bottom-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="mainmenu pull-left">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                <li><a href="index.php" class="active">Beranda</a></li>
                                <li class="dropdown"><a href="">Kamera<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="index.php?kat=1">DSLR</a></li>
                                        <li><a href="index.php?kat=2">MIRORRLES</a>
                                        <li><a href="index.php?kat=3">ACTION CAM</a>
                                        </li> 
                                    </ul>
                                </li> 
                                <li><a href="lensa.php">Lensa</a></li>
                                <li><a href="asessoris.php">Asessoris</a></li>
                                <li><a href="lelang.php">Lelang </a></li>
                                <li><a href="jual.php">Jual</a></li>
                                <li><a href="carasewa.php">Cara Sewa</a></li>
                                <li><a href="contact.php">Kontak</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-bottom-->
    </header><!--/header-->

    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="index.php">Beranda</a></li>
                  <li class="active">Keranjang</li>
                </ol>
            </div>
            
            <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td style= text-align:right class="image">Item</td>
                        <td style= text-align:center class="name">Nama Produk</td>
                        <td style= text-align:center class="quantity"> Kondisi</td>
                        <td style= text-align:center class="quantity"> Deskripsi</td>
                        <td style= text-align:center class="quantity"> Harga Jual</td>
                        <td style= text-align:center class="name"> Status Iklan</td>
                        <td style= text-align:center class="name"> Status Barang</td>
                        <td style= text-align:center class="quantity"> Edit | Hapus</td>
                        
                        <!-- <td></td> -->
                    </tr>
                </thead>
                <tbody>

        
                <?php
               
                // $s = mysqli_query($mycon, "SELECT m.id, n.foto FROM postinganjualkamera n, user m WHERE m.username = '$pengguna'");
                 $s = mysqli_query($mycon, "SELECT b.id, b.namaproduk, b.foto, b.kondisikamera, b.deskripsi, b.hargajual, b.status, b.statusterjual
                                            FROM user p, postinganjualkamera b 
                                            WHERE b.iduser = p.id AND  p.username = '$pengguna' ");
           
                if(!$s)
                {
                    echo 'error s' .mysqli_error($mycon);
                }
                 while($row = mysqli_fetch_assoc($s))
                {
                    echo '
                    <tr>
                        <td class="cart_product">
                           <img src="../bismillah/images/jual/' .$row['foto'].'"  width="70" height="70">
                            

                        <td class="cart_description">
                             <p><p align=center >  <font color="#00008B"> ' .$row['namaproduk']. '</a></p></h4>
                             
                        </td>
                      
                        <td class="cart_description">
                             <h5><p align=center > <font color="#00008B">' .$row['kondisikamera']. '</a></p></h5>
                       </td>
                        <td class="cart_description">
                                     <textarea  style= text-align:left  type="text" name="" disabled /> ' .$row['deskripsi']. '</textarea >
                               </td>
                               
                        <td class="cart_price">
                            
                            <p align=center >Rp.' .number_format($row['hargajual'], 0, ',', '.'). '</p>
                             
                        </td>  
                          <td class="cart_description">
                          
                             <h5><p align=center ><font color="#FF0000">' .$row['status']. '</a></p></h5>
                       </td>
                       
                        <td class="cart_description">
                         <h5><p align=center ><font color="#FF0000">' .$row['statusterjual']. '</a></p></h5>
                             <button type="button" class="btn btn-info btn-sm" data-idk="' .$row['id']. '" data-valik="' .$row['statusterjual']. '"data-toggle="modal" data-target="#statusterjuals"> Tandai Sebagai Terjual</button>
                       </td>

                        <td class="cart_price">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-danger" href="editjual.php?id='.$row['id']. '">
                         <i class="fa fa-pencil fa-fw"></i></a>
                        
                         <a class="btn btn-danger" href="proseseditjual.php?idhapus='.$row['id']. '">
                         <i  class="fa fa-trash-o fa-lg"></i></a>
                
                        </td>
                        
                    </tr>';
                    
                } 

                 //mysqli_close($mycon); // menutup koneksi ke database

                    ?>
                       <div class="modal fade" id="statusterjuals" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Status Barang</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Status Barang</label><br>
                                                    <form method="POST" action="iklanku.php">
                                                    <input type="hidden" name="idnotas" id="idnotas">
                                                    
                                                    <!-- <input type="text" id="tst"> -->

                                                    <select id="status" name="status" class="form-control">
                                                        <option value="">Belum Diset</option>
                                                        <option value="belom terjual">Belom Terjual</option>
                                                        <option value="terjual">Kembali</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="updts" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                            <script type="text/javascript"> 
                            $("#statusterjuals").on('show.bs.modal', function(e) {
                                var valida = $(e.relatedTarget).data('valik');
                                var idnot = $(e.relatedTarget).data('idk');
                                 
                                // $("#tst").val(valida);  
                                $("#status").val(valida);  
                                $("#idnotas").val(idnot);                                 
                            });
                            </script>
                </tbody>
            </table>
            </div>
        
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
               
                <div class="col-sm-6">
                    
                        
                </div>
            </div>
        </div>
    </section><!--/#do_action-->


    <footer id="footer"><!--Footer-->
        <div class="footer-widget">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="single-widget">
                            <h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera Probolinggo</h1><br>
                            <h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
                            <br></br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer><!--/Footer-->
    


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>



<?php
if(isset($_POST['updts']))
{
    $notas = $_POST['idnotas'];
    $statuss = $_POST['status'];
    //$stokkamera = '';

    if($statuss == '')
    {
        //do nothing, redirect ke masorder.php
        echo '<script language="javascript"> 
          document.location.href="iklanku.php"
          </script>';
    }
    elseif ($statuss == 'belom terjual') {

        echo '<script language="javascript"> 
          alert("status tidak")
          </script>';
        //update jadi menunggu bukti transfer
        //(jaga jaga ketika setelah diupdate valid dan ingin dikembalikan menjadi tidak valid
           
        $r = mysqli_query($mycon, "UPDATE postinganjualkamera SET statusterjual = 'belom terjual' WHERE id");

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
         document.location.href="iklanku.php"
          </script>';
    }
    else
    {
      echo '<script language="javascript"> 
          alert("status terjual")
          </script>';
 $p = mysqli_query($mycon, "SELECT * FROM user");
            $res_p = mysqli_fetch_array($p);
            $idkamera = $res_p['id'];

       // $e = mysqli_query($link, "UPDATE kamera SET gambar = '" .$spl. "' WHERE id = '" .$id. "'");
            $q = mysqli_query($mycon, "UPDATE postinganjualkamera set statusterjual = 'terjual ' where id = '".$notas."' AND id = '".$idkamera."'");
               if(!$q)
                 {
                    echo 'error ' .mysqli_error($mycon);
                 }
        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="iklanku.php"
          </script>';
    }
}

?>