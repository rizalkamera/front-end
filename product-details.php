<?php 
include 'koneksi.php';
session_start(); 

if(empty($_SESSION['aktif']))
{
	echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Product Details | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php include 'header.php' ?>
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Kategori</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="index.php?kat=1">DSLR</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="index.php?kat=2">MIRORLES</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					</div>
				</div>
				<?php
				include 'koneksi.php';

				$itemID = 0;

				if(isset($_GET['itemID']))
				{
					$item_id = $_GET['itemID'];
				}

				$sql = mysqli_query($mycon, "SELECT * FROM kamera WHERE id = '" .$item_id. "'");


				?>
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<?php
								while($row = mysqli_fetch_array($sql))
								{

									echo '<img src="../bismillah/images/' .$row['gambar']. '" alt="" /></a>';
								}
								?>
							</div>
						</div>
						
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<?php
								$sql = mysqli_query($mycon, "SELECT *, k.id as kamera_id FROM kamera k, kategori kat WHERE k.kategori_id = kat.id and k.id = '" .$item_id. "'");
								while($row = mysqli_fetch_array($sql))
								{
						  echo '<form method="POST" action="cart_proc1.php">

									<h2>' .$row['namakamera']. '</h2>

								 <label>Tanggal Pengambilan</label>
								 <input type="datetime-local" class="form-control" id="datefield" name="ambil" 
								 required min="'.strftime('%Y-%m-%dT%H:%S', time()+(5*3600)).'"/><br>';

																
       								if($row['stoktotal'] > 0)
									{
										?>
								<select class="form-control" id="cmbdurasi" name="harga" required >
								   <option value="">Pilih Durasi Sewa</option>
								   <option value="<?php echo $row['harga_6jam'].'-1'; ?>">6 jam </option>
								   <option value="<?php echo $row['harga_12jam'].'-2'; ?>">12 jam </option>
								   <option value="<?php echo $row['harga_24jam'].'-3'; ?>">24 jam </option>
								</select><br>

								<input type="hidden" name="item_ID" value="<?php echo $row['kamera_id']; ?>"><br>
							    <center>Harga per Item :<h4 id="hargasewa1">Rp.xxx.xxx,-</h4></center><br>

								<label>Quantity:</label> &nbsp;&nbsp;&nbsp;&nbsp;
							  	<input type="number" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" 
							  			min="1" max="<?php echo $row['stoktotal']; ?>" name="jml_item" value="1" required/><br></br>
							      
							    <!-- jika pilih opsi 1, maka durasinya 6jam (lihat angka yang diconcat di value cbo)
							    jika pilih opsi 2, maka durasinya 12jam (lihat angka yang diconcat di value cbo)
							    jika pilih opsi 3, maka durasinya 24jam (lihat angka yang diconcat di value cbo) -->
							   

							    <input type="submit" class"btn btn-default" 
							    		style="background: #FE980F; border: 0 none; border-radius: 20; color: #FFFFFF; font-size: 15px; margin-bottom: 10px; width: 100%;"  
							    		name="sbmt_form" value="Add to Cart" />
							  			
								</form>
								<p><b>Stok Barang : </b> Tersedia</p>
									<?php
									}



									else
									{
											?>

								<select class="form-control" id="cmbdurasi" name="durasi" disabled >
								   <option value="0">Pilih Durasi Sewa</option>
								   <option value="'.$row['harga_6jam'].'">6 jam </option>
								   <option value="'.$row['harga_12jam'].'">12 jam</option>
								   <option value="'.$row['harga_24jam'].'">24 jam</option>
								</select>

								<label>Quantity:</label> &nbsp;&nbsp;&nbsp;&nbsp;
							  	<input type="number" class="form-control" onkeypress="return event.charCode >= 48" min="1" max="' .$row['stok']. '" disabled><br></br>
							      
							    


							    <input type="hidden" name="item_ID" value="' .$row['kamera_id'].'"><br>
							    <center><h4>Rp.xxx.xxx,-</h4></center><br>

							    <input type="submit" class"btn btn-default" 
							    		style="background: #FE980F; border: 0 none; border-radius: 20; color: #FFFFFF; font-size: 15px; margin-bottom: 10px; width: 100%;"  
							    		name="sbmt_form" value="Add to Cart" disabled/>
							  			
								</form>
									
								<p><b>Stok Barang : </b>Habis</p>
								<?php
									}
									
								}
								?>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li><a href="#details" data-toggle="tab">Details</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="details" >
								<?php
								$sql = mysqli_query($mycon, "SELECT * FROM kamera WHERE id = '" .$item_id. "'");
								while($row = mysqli_fetch_array($sql))
								{
									echo '<p>' .$row['deskripsi'] .'<p>';
								}
								?>
							</div>
						</div>
					</div><!--/category-tab-->
				</div>
			</div>
		</div>
	</section>

<!-- ------------SCRIPT SET HARGA SAAT PILIH DURASI (ONCHANGE)----------- -->
	<script type="text/javascript">
		var mycbo = document.getElementById('cmbdurasi');
		var hargasewa2 = document.getElementById('hargasewa1');
		var temp = '';
		var temp2 = '';

		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}

		mycbo.onchange = function(){
			// alert(this.value);
			// alert(formatRupiah(this.value, 'Rp. '));
			temp = this.value;
			temp2 = temp.substring(0, (temp.length-2))
			document.getElementById("hargasewa1").innerHTML = formatRupiah(temp2, 'Rp.');
		}
	</script>
<!-- ------------SCRIPT SET HARGA SAAT PILIH DURASI (ONCHANGE)----------- -->

<script text="text/javascript">
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm+'-'+dd;
document.getElementById("datefield").setAttribute("max", today);
<script>
	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretanny</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>