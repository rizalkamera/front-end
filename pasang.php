<?php
include 'koneksi.php';
session_start();?>

<!DOCTYPE html>
<html lang="en">

<?php
include 'koneksi.php';
//echo $_SESSION['aktif'];
if (empty($_SESSION['aktif'])) {
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
} else if (isset($_SESSION['aktif'])) {
    $pengguna = $_SESSION['aktif'];
}

// //adakah transaksi yang sedang diproses (yang statusnya bukan selesai)
// $s = mysqli_query($mycon, "SELECT * FROM pelanggan p, nota_jual n WHERE p.id_pel = n.pelanggan_id AND p.username = '$pengguna' AND n.status_pesanan != 'selesai' AND p.hapuskah = '0' AND n.hapuskah = '0'");
// while ($q = mysqli_fetch_array($s)) {
//     $stat  = $q['status_pesanan'];
//     $valid = $q['bukti_valid'];
//     $bukti = $q['bukti_transfer'];
// }

// $cek_data = mysqli_num_rows($s);
// //jika tidak ada, ada 2 kemungkinan
// //1. pelanggan belum pernah melakukan transaksi sama sekali
// //2. semua transaksi yang dilakukan sudah selesai
// if ($cek_data < 1) {
// 	//cek status transaksi yang selesai, urutkan berdasarkan tanggal
// 	$s = mysqli_query($mycon, "SELECT * FROM pelanggan p, nota_jual n WHERE p.id_pel = n.pelanggan_id AND p.username = '$pengguna' AND n.status_pesanan = 'selesai' AND p.hapuskah = '0' AND n.hapuskah = '0'");
// 	$row = mysqli_num_rows($s);

// 	//jika jml baris < 1, artinya pelanggan belum pernah transaksi sama sekali
// 	if ($row < 1) {
// 		echo '<script language="javascript">';
// 	    echo 'document.location.href="checkout_empty.php"';
// 	    echo '</script>';
// 	}
// 	else{
// 		echo '<script language="javascript">';
// 	    echo 'document.location.href="success.php"';
// 	    echo '</script>';
// 	}
// }
// //jika ada
// else {
//     //cek status transaksinya
//     if ($stat == 'menunggu selesai belanja') {
//         echo '<script language="javascript">';
//         echo 'document.location.href="checkout_notproceed.php"';
//         echo '</script>';
//     } 
//     else if ($stat == 'menunggu bukti transfer') {
//         //cek apakah yang menunggu bukti transfer itu status buktinya kosong atau terisi 0

//         //kalo kosong, artinya pelanggan belum mengupload bukti transfer, atau sudah mengupload tapi belum divalidasi
//     	//jika status bukti kosong dan bukti transfer belum terisi, maka pelanggan belum melakukan upload
//     	//jika status bukti kosong tapi bukti transfer sudah terisi, maka pelanggan sudah melakukan upload namun belum divalidasi oleh admin

//         //kalo terisi 0, artinya bukti transfer yang diupload tidak valid, dan pelanggan harus mengu
//         if ($valid == "") {
//         	//jika status bukti kosong dan bukti transfer kosong, maka alihkan ke checkout_done.php
//         	if ($bukti == '') {
//         		echo '<script language="javascript">';
// 	            echo 'document.location.href="checkout_done.php"';
// 	            echo '</script>';
//         	}

//         	//jika status bukti kosong dan bukti transfer terisi, maka alihkan ke on_process.php
//         	else{
//         		echo '<script language="javascript">';
// 	            echo 'document.location.href="on_process.php"';
// 	            echo '</script>';
//         	}
//         }
//         elseif ($valid == "0") {
//         	echo '<script language="javascript">';
//             echo 'document.location.href="reupload.php"';
//             echo '</script>';
//         }
//     } 
//     else if ($stat == 'proses pengiriman') {
//         echo '<script language="javascript">';
//         echo 'document.location.href="proses_kirim.php"';
//         echo '</script>';
   // } 
//}


?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- untuk radiobutton event -->
	<script src="js/jquery.min.js"></script>
	<!-- untuk radiobutton event -->

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Checkout | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">

</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h1><font color="#9C9B9B">Kurnia  </font><font color="#FE980F">Kamera</font></h1></a>
						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
					            		<li><a href="checkout.php">
					            			<div class="mark">Pemesanan Barang</div>
					            		</a></li>
					            		<li><a href="checkout_lelang.php">
					            			<div class="mark">Transaksi Lelang</div>
					            		</a></li> 
					            	</ul>
					          	</li>
								<li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
								<?php 
								/*if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}*/
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="step">
						<ul class="steps">
						  <li class="select">1. Pembayaran</li>
						  <li >2. Konfirmasi Pembayaran</li>
						  <li >3. Upload Bukti Transfer</li>
						  <li >4. Selesai</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->


	<section id="cart_items">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="cust">
						<p>Data Penerima Barang</p>
						<form method="POST" action="">
							<input type="text" name="nama" placeholder="Nama Lengkap" required/>
							<input type="text" name="nohp" placeholder="No HP" required/><br>
							<select style="width: 60%" id="cboalamat" name="cboalamat" required>
								<option value="" selected>--Pilih Alamat Pengiriman--</option>
								<?php
								$sq = mysqli_query($mycon, "SELECT alamat from pelanggan WHERE username = '$pengguna'");
								while ($row1 = mysqli_fetch_array($sq)) {
								    echo '<option value="' . $row1['alamat'] . '">Alamat Anda</option>';
								}
								?>
								<option value="aa">Alamat Lain</option>
							</select>
							<textarea name="alamat" id="alamat" rows="2" cols="1" disabled required></textarea>
							<input type="text" name="kodepos" placeholder="Kode Pos" required>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="cust">
						<p> Pilih Paket Pengiriman</p>
						<input type="radio" name="paket" value="19000" required> Regular  <span style="font-size: 14px;">(2-3 hari)</span><br>
						<input type="radio" name="paket" value="21000" required> YES  <span style="font-size: 14px;">(1 hari)</span><br>
						<!-- tutup form ada dibawah biaya stelah button lanjutkan -->
					</div>
				</div>
			</div>
			<div class="review-payment">
				<h2>Tinjau Ulang Barang dan Biaya Total</h2>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<?php
					$sql = mysqli_query($mycon, "SELECT b.nama_barang, b.jpeg_file, b.harga, b.stok, h.nota_id, h.barang_id, h.jml_jual, h.harga_jual,
										SUM(h.jml_jual*h.harga_jual) as subtotal FROM pelanggan p, nota_jual n, hub_nota_barang h, barang b 
										WHERE p.id_pel = n.pelanggan_id AND n.id_notaJual = h.nota_id AND h.barang_id = b.id_barang AND 
										p.username = '$pengguna' AND n.status_pesanan = 'menunggu pembayaran' AND n.hapuskah = '0' group by 
										b.nama_barang");
					while ($row = mysqli_fetch_array($sql)) {
					    echo '
						<tr>
							<td class="cart_product">
								<a href=""><img src="images/barang/cart/' . $row['jpeg_file'] . '" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">' . $row['nama_barang'] . '</a></h4>
							</td>
							<td class="cart_price">
								<br>
								<p>Rp. ' . number_format($row['harga'], 0, ',', '.') . ',-' . '</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<br>
									<p>' . $row['jml_jual'] . '</p>
								</div>
							</td>
							<td class="cart_total">
								<br>
								<p class="cart_total_price">Rp. ' . number_format($row['subtotal'], 0, ',', '.') . ',-' . '</p>
							</td>
						</tr>';
					}?>
						<tr class="checkout_akumulasi">
							<td colspan="3"></td>
								<?php
								$sq = mysqli_query($mycon, "SELECT SUM(s.subtotal) as total from (SELECT b.nama_barang, b.jpeg_file, b.harga, b.stok, h.nota_id, h.barang_id, h.jml_jual, h.harga_jual, SUM(h.jml_jual*h.harga_jual) as subtotal FROM pelanggan p, nota_jual n, hub_nota_barang h, barang b WHERE p.id_pel = n.pelanggan_id AND n.id_notaJual = h.nota_id AND h.barang_id = b.id_barang AND p.username = '" . $pengguna . "' AND n.status_pesanan = 'menunggu pembayaran' AND n.hapuskah = '0' group by b.nama_barang ) AS s");
								while ($row1 = mysqli_fetch_array($sq)) {
								    //buat input type hidden yang nyimpen total
								    echo '
								<input type="hidden" id="total" value="' . $row1['total'] . '" />
								<td>Total Belanja</td>
								<td>Rp. ' . number_format($row1['total'], 0, ',', '.') . ',-</td>';
								}?>
						</tr>
						<tr class="checkout_akumulasi">
							<td colspan="3"></td>
								<td>Biaya Kirim</td>
						 		<td>Rp. <span class="tampil"></span></td>
						</tr>
						<tr class="checkout_akumulasi">
							<td colspan="3"></td>
								<td>Biaya yang Harus Dibayar</td>
								<td>Rp. <span class="tampil_total"></span></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-sm-4 col-sm-offset-5">
				<div class="btn">
					<button type="submit" name="submit">Beli Produk Ini</button>
					</form>
					<br></br>
					<br></br>
				</div>
			</div>
		</div>

	</section> <!--/#cart_items-->


	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Commis F&B</h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Coffee and Coffee Machine Distributors</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->

    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>

    <script src="js/jquery.min.js"></script>
	<script>

	//*********set ongkir dkk***********
	$(document).on("change","input[type=radio]",function(){
		//set value radiobutton yang dipilih
	    var ongkir=$('[name="paket"]:checked').val();

	    //buat method format rupiah
	    function currency(str) {
	    	var	reverse = str.toString().split('').reverse().join(''),
			ribuan 	= reverse.match(/\d{1,3}/g);
			ribuan	= ribuan.join('.').split('').reverse().join('');

			return ribuan;
	    }

	    //set format rupiah nya ongkir
	    var ongkir_fix = currency(ongkir);
	   	ongkir_fix = ongkir_fix + ",-";

		//ambil total belanja
	    var tot = document.getElementById('total').value;

	    // convert ongkir dan total ke int
	    var ongkir_num = ongkir*1;
	    var tot_num = tot*1;


	    //tambahkan value radiobutton dengan total
	    var total = ongkir_num + tot_num;

	    //set format rupiahnya total
	    var total_fix = currency(total);
	    total_fix = total_fix + ",-";

		//tampilkan ke html
		$(".tampil").html(ongkir_fix); //ongkir
	    $(".tampil_total").html(total_fix);
	    // $(".total").html(total);
	});
	//**************end of set ongkir dkk**************


	//*********set alamat************
	var alamat = document.getElementById('alamat');
	var mycbo = document.getElementById('cboalamat');

	mycbo.onchange = function(){
		//jika user memilih comboboc alamat lain (alamat lain valuenya aa),
		//maka textarea di disabled false (diaktifkan)
	     if (mycbo.value === "aa") {
	     	alamat.value = "";
	        document.getElementById('alamat').disabled='';
	    } else {
	    	alamat.value = "";
	     	alamat.value = alamat.value + this.value;
	    	document.getElementById('alamat').disabled='true';
	    }
	}
	//**********end of set alamat**********8
	</script>
</body>
</html>


<?php

// lalu ada link "lanjutkan transaksi" yang menuju ke checkout_done.php

if (isset($_POST['submit'])) {
    $nama = $_POST['nama'];
    $noHP = $_POST['nohp'];

    //awalnya simpan value alamat dari combobox
    $alamat = $_POST['cboalamat'];

    $kodepos = $_POST['kodepos'];
    $paket   = $_POST['paket'];

    //tapi kalo value nya "aa", artinya user memilih alamat lain.
    //jadi ambil value dari text area
    if ($alamat == 'aa') {
        $alamat = $_POST['alamat'];
    }

    if ($paket == 19000) {
        $tipe_paket = 'Reguler';
    } else {
        $tipe_paket = 'YES';
    }

	//ambil nota jual terkait
    $s = mysqli_query($mycon, "SELECT n.id_notaJual FROM nota_jual n, pelanggan p WHERE n.pelanggan_id = p.id_pel AND p.username = '" . $pengguna . "' AND n.status_pesanan = 'menunggu pembayaran' AND p.hapuskah = '0' AND n.hapuskah = '0'");
    while ($res = mysqli_fetch_array($s)) {
        $id_nota = $res['id_notaJual'];
    }

    //insert data pengiriman
    $sql = mysqli_query($mycon, "INSERT INTO pengiriman(nama_penerima, tipe_pengiriman, noHP_penerima, alamat_penerima, kodePos, biaya_kirim, notaJual_id, hapuskah) VALUES('$nama','$tipe_paket','$noHP','$alamat','$kodepos','$paket','$id_nota','0')");

    if(!$sql)
    {
    	echo mysqli_error($mycon);
    }


    //set grand_total
    //ambil total belanja saat ini
    $q = mysqli_query($mycon, "SELECT total_belanja FROM nota_jual WHERE id_notaJual = '" . $id_nota . "' AND hapuskah = '0'");
    while ($w = mysqli_fetch_array($q)) {
        $belanja = $w['total_belanja'];
    }

    $grand = $belanja + $paket;

    //update grand total
    $sql2 = mysqli_query($mycon, "UPDATE nota_jual SET grand_total = '" . $grand . "' WHERE id_notaJual = '" . $id_nota . "'");

    //set tanggal nota
    date_default_timezone_set('Asia/Bangkok');
    $tgl  = date('Y-m-d H:i:s');
    $sql3 = mysqli_query($mycon, "UPDATE nota_jual SET tanggal_notaJual = '" . $tgl . "' WHERE id_notaJual = '" . $id_nota . "'");

    //set status transaksi jadi menunggu bukti transfer
    $e = mysqli_query($mycon, "UPDATE nota_jual SET status_pesanan = 'menunggu bukti transfer' WHERE id_notaJual = '" . $id_nota . "'");

    //set jenis_bayar jadi transfer
    $po = mysqli_query($mycon, "UPDATE nota_jual SET jenis_bayar = 'transfer' WHERE id_notaJual = '" . $id_nota . "'");

    

    echo '<script language="javascript">' .
        'window.alert("Data Penerima barang telah terinput. Anda dapat melanjutkan ke proses selanjutnya.");' .
        'document.location.href="checkout_done.php"' .
        '</script>';
}

?>