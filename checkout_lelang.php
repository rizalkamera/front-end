<?php 
include 'koneksi.php';
session_start(); ?>

<!DOCTYPE html>
<html lang="en">

<?php 
// include 'koneksi.php';
//echo $_SESSION['aktif'];
if(empty($_SESSION['aktif']))
{
	echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="login.php"';
    echo '</script>';
}
else if(isset($_SESSION['aktif']))
{
	$pengguna = $_SESSION['aktif'];
}



//adakah transaksi yang sedang diproses
$s = mysqli_query($mycon,
"SELECT * FROM lelang l, user p 
WHERE l.pelanggan_id = p.id AND l.statuspembayaran != '' AND p.username = '" .$pengguna. "' AND l.hapuskah = '0' AND p.hapuskah = '0'");
//$s = mysqli_query($mycon, "SELECT * FROM user p, lelang l WHERE p.id = l.pelanggan_id AND p.username = '$pengguna' AND l.statuspembayaran != '' AND p.hapuskah = '0' AND l.hapuskah = '0'");
$res_s = mysqli_fetch_array($s);
$stat = $res_s['statuspembayaran'];
$bukti = $res_s['buktitransfer'];
$valid = $res_s['buktivalid'];

$cek_data = mysqli_num_rows($s);
// //jika tidak ada 
if($cek_data < 1)
{
	echo '<script language="javascript">';
	//echo 'window.alert("Data berhasil terinput. Klik OK untuk Masuk.");';
    echo 'document.location.href="checkout_lelang_empty.php"';
    echo '</script>';
}
//jika ada
else
{
	//cek status transaksinya
	if($stat == 'menunggu bukti transfer')
	{
		//cek apakah yang menunggu bukti transfer itu status buktinya kosong atau terisi 0
        //kalo kosong, artinya pelanggan belum mengupload bukti transfer
        //kalo terisi 0, artinya bukti transfer yang diupload tidak valid, dan pelanggan harus menunggu
        if ($valid == "") {
        	//jika status bukti kosong dan bukti transfer kosong, maka alihkan ke checkout_done.php
        	if ($bukti == '') {
        		echo '<script language="javascript">';
	            echo 'document.location.href="checkout_lelang_done.php"';
	            echo '</script>';
        	}
        	//jika status bukti kosong dan bukti transfer terisi, maka alihkan ke on_process.php
        	else{
        		echo '<script language="javascript">';
	            echo 'document.location.href="on_process.php"';
	            echo '</script>';
        	}
        }
        elseif ($valid == "0") {
        	echo '<script language="javascript">';
            echo 'document.location.href="reupload_lelang.php"';
            echo '</script>';
        }
	}
	else if($stat == 'selesai')
	{
		echo '<script language="javascript">';
        echo 'document.location.href="success_lelang.php"';
        echo '</script>';
	}
} ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="css/font-awesome.min.css">	

	<!-- untuk radiobutton event -->
	<script src="js/jquery.min.js"></script>
	<!-- untuk radiobutton event -->

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Checkout | Kurnia Kamera</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><h1><font color="#9C9B9B">Kurnia  </font><font color="#FE980F">Kamera</font></h1></a>
						</div>	
					</div>
					<div class="col-sm-5 col-sm-offset-3">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="dropdown"><a href=""><i class="fa fa-money"></i> Pembayaran<i class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
					            		<li><a href="checkout.php">
					            			<div class="mark">Pemesanan Barang</div>
					            		</a></li>
					            		<li><a href="checkout_lelang.php">
					            			<div class="mark">Transaksi Lelang</div>
					            		</a></li> 
					            	</ul>
					          	</li>
								<li class="dropdown"><a href="cart.php"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
								<?php 
								if(isset($_SESSION['aktif']))
								{
						echo '	<li class="dropdown"><a href=""><i class="fa fa-user"></i> ' .$pengguna. '</a>
									<ul role="menu" class="sub-menu">
			                    		<li><a href="logout.php">
			                    			<div class="mark">Logout</div>
			                    		</a></li>  
			                    	</ul>
			                  	</li>';
								}
								else if(empty($_SESSION['aktif']))
								{
						echo '	<li><a href="login.php"><i class="fa fa-lock"></i> Login</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="step">
						<ul class="steps">
						  <li class="select">1. Pembayaran </li>
						 
						  <li >2. Upload Bukti Transfer</li>
						  <li >3. Selesai</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	
	<section id="cart_items">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
				
					<div class="review-payment">
				<h2>Tinjau Ulang Barang dan Biaya Total </h2>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td style= text-align:center class="image">Produk</td>
							<td style= text-align:center class="description">Nama Barang</td>
							
							<td style= text-align:center class="total">Biaya Bayar</td>
							<td></td>
						</tr>
					</thead>
					



			<!-- <div class="review-payment col-sm-6 col-sm-offset-2">
				<h2>Tinjau Ulang Barang dan Biaya Total</h2>
			</div>
			<div class="table-responsive cart_info col-sm-6 col-sm-offset-2">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="description">BARANG</td>
							<td class="price">Biaya yang harus Dibayarkan</td>
							<td></td>
						</tr>
					</thead>
					<tbody> -->
					<?php
					$sql = mysqli_query($mycon,"SELECT * FROM lelang l, user p WHERE l.pelanggan_id = p.id AND l.statuspembayaran = 'menunggu pembayaran' AND p.username = '" .$pengguna. "' AND l.hapuskah = '0' AND p.hapuskah = '0'");
					while($row = mysqli_fetch_array($sql))
					{
						echo '

						<tr>
							<td class="cart_total">
								<p align=center <a href=""> <img src="../bismillah/images/' .$row['gambarbarang']. '" width="100" height="100" /></a></p>
							</td>
							<td class="cart_total">

								<h4 align=center > ' . $row['namabarang'] . '</a></h4>
								
							</td>
							<td class="cart_total">
								<h4 align=center>Rp. ' .number_format($row['hargatertinggi'], 0, ',', '.'). ',-'. '</h4>
							</td>
							<td class="cart_total">
						
						</tr>';
					} ?>
						
					</tbody>
				</table></div>
				<form method="POST" action="checkout_lelang.php">
				<input type="checkbox" required /> Data yang saya masukkan telah terisi dengan benar. Dan saya ingin melanjutkan ke proses Pembayaran.
			
			<div class="col-sm-4 col-sm-offset-3">
				<div class="btn">
					
					<button type="submit" name="sbmt">Lanjutkan ke Pembayaran</button>
					</form>
					<br></br>
					<br></br>
				</div>
			</div>
		</div>
		
	</section> <!--/#cart_items-->
	

	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="single-widget">
							<h1 style="text-align: center; color: #9C9B9B;">Kurnia Kamera </h1><br>
							<h4 style="text-align: center; color: #9C9B9B;">Sewa Kamera Semudah Jepretannya</h4>
							<br></br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->

    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>

    <script src="js/jquery.min.js"></script>
	<script>

	//*********set alamat************
	var alamat = document.getElementById('alamat');
	var mycbo = document.getElementById('cboalamat');

	mycbo.onchange = function(){
		//jika user memilih comboboc alamat lain (alamat lain valuenya aa),
		//maka textarea di disabled false (diaktifkan) 
	     if (mycbo.value === "aa") {
	     	alamat.value = "";
	        document.getElementById('alamat').disabled='';
	    } else {
	    	alamat.value = "";
	     	alamat.value = alamat.value + this.value;
	    	document.getElementById('alamat').disabled='true';
	    }
	}
	//**********end of set alamat**********8
	</script>

	<script>
	
	$(function(){
	  $('.countdown').each(function(){
	    $(this).countdown($(this).attr('value'), function(event) {
	      if (event.strftime('%D') == 0) 
	      {
	        if (event.strftime('%H') == 0) 
	        {
	          if (event.strftime('%M') == 0) 
	          {
	            if (event.strftime('%S') == 0) 
	            {
	            	var id = document.getElementById('idlel').value;
	            	$.ajax({
	            		type: "POST",
	            		url: "update_lelang.php",
	            		data: 'id=' +id,
	            		success: function (html){
	            			$(this).text('EXPIRED');
	            		}
	            	});
	            }
	            else
	            {
	            	// var id = document.getElementById('idlel').value;
	            	// $(this).text(id);
	              $(this).text(event.strftime('%S detik'));
	            }
	          }
	          else
	          {
	            $(this).text(event.strftime('%M menit %S detik'));
	          }
	        }
	        else
	        {
	          $(this).text(event.strftime('%H jam %M menit %S detik'));
	        }
	      }
	      else
	      {
	        $(this).text(event.strftime('%D hari %H jam %M menit %S detik'));
	      }
	    });
	  });
	});
	</script>
	<!-- ******** END OF SCRIPT COUNT DOWN TIMER********* -->

</body>
</html>


<?php

// saat beli produk ini di klik, simpan data yang di inputkan

// setelah selesai tersimpan, kirim email ke email pengguna. isinya :
// <p>PERHATIAN : setelah anda melakukan transfer ke rekening di bawah ini, mohon simpanlah bukti transfer yang anda dapatkan. Karena sistem akan meminta anda untuk mengupload foto bukti transfer anda.</p>
// <div class="col-sm-8 col-sm-offset-2">
// 	<div class="detil_bayar">
// 		<p>Total transaksi yang harus anda bayar adalah</p>
// 		<center><strong><p>query total</p></strong></center>
// 		<br>
// 		Transfer ke : BCA (088-54847-3849)
// 		<p class="rekening">a/n : Commis F&B</p>

// lalu ada link "lanjutkan transaksi" yang menuju ke checkout_done.php


if(isset($_POST['sbmt']))
{
	//ambil id lelang yang mau diupdate
	$s = mysqli_query($mycon, "SELECT l.id FROM lelang l, user p WHERE l.pelanggan_id = p.id AND p.username = '" .$pengguna. "' AND l.statuspembayaran = 'menunggu pembayaran' AND p.hapuskah = '0' AND l.hapuskah = '0'");
	while ($res = mysqli_fetch_array($s)) 
	{
		$id_lelang = $res['id'];
	}
	
	//update email di lelang resep terkait dan status transaksi jadi menunggu bukti pembayaran
	$sql1 = mysqli_query($mycon, "UPDATE lelang SET statuspembayaran = 'menunggu bukti transfer' WHERE id = '" .$id_lelang. "'");

	if (!$sql1) {
		echo 'error ' .mysqli_error($mycon);
	}

	echo '<script language="javascript">'. 
		'window.alert("Data Penerima barang telah terinput. Anda dapat melanjutkan ke proses selanjutnya.");'.
		'document.location.href="checkout_lelang_done.php"'.
		'</script>';

	// echo '<script language="javascript">'. 
	// 	'window.alert("testtttt");'.
	// 	'</script>';
}

?>